package com.moviedick;
import java.io.IOException;

public class OpenBrowser {


    //NAVEGADOR POR DEFECTO POR SISTEMA
    private void abrirNavegadorPredeterminadorWindows(String url) throws IOException {
        Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
    }

    private void abrirNavegadorPredeterminadorLinux(String url) throws IOException {
        Runtime.getRuntime().exec("xdg-open " + url);
    }

    private void abrirNavegadorPredeterminadorMacOsx(String url) throws IOException {
        Runtime.getRuntime().exec("open " + url);
    }

    //NAVEGADOR POR DEFECTO GENERICO
    public void abrirNavegadorPorDefecto(String url) throws IOException {
        String osName = System.getProperty("os.name");
        if (osName.contains("Windows"))
            abrirNavegadorPredeterminadorWindows(url);
        else if (osName.contains("Linux"))
            abrirNavegadorPredeterminadorLinux(url);
        else if (osName.contains("Mac OS X"))
            abrirNavegadorPredeterminadorMacOsx(url);
        else { //INFORMAR SISTEMA NO SOPORTADO }
        }
    }
}