package com.moviedick;

import com.moviedick.fx.fxml.portada.utilidades.MainViewModel;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.util.UtilBanner;
import com.moviedick.fx.util.UtilFecha;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoadFilm {
    private List<Pelicula> peliculas;
    private int index;
    private final int MAX = 32;

    public LoadFilm(List<Pelicula> peliculas, int index) {
        this.peliculas = peliculas;
        this.index = index;
    }

    public void cargarItems() {
        FlowPane container = MainViewModel.MainViewModel().getContainer();
        container.setStyle("-fx-background-color: black");
        container.setAlignment(Pos.TOP_LEFT);
        container.getChildren().clear();
        List<AnchorPane> a = new ArrayList<>();
        int max;
        if (index + MAX > peliculas.size()) {
            max = peliculas.size();
        } else {
            max = index + MAX;
        }
        if(max==0){
            try {
                AnchorPane item1 = FXMLLoader.load(getClass().getResource("./fx/fxml/portada/EmptyFilm.fxml"));
                container.setAlignment(Pos.CENTER);
                container.getChildren().add(item1);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i = index; i < max; i++) {
            int finalI = i;
            AnchorPane item1 = null;
            try {
                item1 = FXMLLoader.load(getClass().getResource("./fx/fxml/portada/AdapterPelicula.fxml"));
                container.getChildren().add(item1);
                a.add(item1);

            } catch (IOException e) {
                e.printStackTrace();
            }
            AnchorPane finalItem = item1;
            List<Pelicula> finalPeliculas = peliculas;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    addItemPelicula(finalPeliculas.get(finalI), finalItem);
                }
            }).start();
        }

        if(max>MAX){
            Button anterior=new Button("PAGINA ANTERIOR");
            ButtonStyle(anterior);
            ButtonOnClickAnterior(anterior);
            anterior.setStyle("-fx-text-fill: white;-fx-background-color: #1D171F ; -fx-border-radius: 10;-fx-background-radius: 10");

            container.getChildren().add(anterior);
        }
        if(max<peliculas.size()){
            Button siguiente=new Button("PAGINA SIGUIENTE");
            ButtonStyle(siguiente);
            ButtonOnClickSiguiente(siguiente);
            siguiente.setStyle("-fx-text-fill: white;-fx-background-color: #1D171F ; -fx-border-radius: 10;-fx-background-radius: 10");

            container.getChildren().add(siguiente);
        }
    }

    private void ButtonOnClickAnterior(Button button) {
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                index-=MAX;
                cargarItems();
            }
        });
    }

    private void ButtonOnClickSiguiente(Button button) {
        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                index+=MAX;
                cargarItems();
            }
        });
    }

    private void ButtonStyle(Button button) {
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-text-fill: white;-fx-background-color: #444548 ; -fx-border-radius: 10;-fx-background-radius: 10");

            }
        });
        button.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-text-fill: white;-fx-background-color: #1D171F ; -fx-border-radius: 10;-fx-background-radius: 10");

            }
        });
    }

    private void addItemPelicula(Pelicula pelicula, AnchorPane item1) {
        FlowPane container = MainViewModel.MainViewModel().getContainer();
        container.setStyle("-fx-background-color: black");
        ImageView img = (ImageView) item1.getChildren().get(0);
        Label genero = (Label) item1.getChildren().get(1);
        Label titulo = (Label) item1.getChildren().get(2);
        Label fecha = (Label) item1.getChildren().get(3);
        String generoString = "";
        for (Genero tmp : PeliculasGenerosDao.connect().listAllFilterGenerosByIdPelicula(pelicula.getIdPelicula())
        ) {
            generoString += tmp.getNombre() + " ";
        }
        String finalGeneroString = generoString;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                genero.setText(finalGeneroString);
                titulo.setText(pelicula.getTitulo());
                titulo.autosize();
                titulo.setStyle("-fx-font-weight: bold");
                fecha.setText(pelicula.getFecha().getYear() + "");
            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Image tmp = new Image(pelicula.getImagenPortada());
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                            img.setImage(tmp);
                            item1.setMinSize(165,245);
                            img.fitWidthProperty().bind(item1.widthProperty());
                            img.fitHeightProperty().bind(item1.heightProperty());

                        }
                    });

                } catch (Exception e) {
                }
            }
        }).start();


        item1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cargaOpcionPelicula("./fx/fxml/portada/Screen1.fxml",pelicula,finalGeneroString);
            }
        });


    }

    private void cargaOpcionPelicula(String fxml,Pelicula pelicula,String generos) {
        FlowPane container = MainViewModel.MainViewModel().getContainer();
        AnchorPane pane=null;
        try {
            pane = FXMLLoader.load(getClass().getResource(fxml));
            container.getChildren().clear();
            container.getChildren().add(pane);
        } catch (IOException e) {
        }
        if(pane!=null){

            ImageView imgFondo = (ImageView) pane.getChildren().get(0);
            Label descripcion = (Label) pane.getChildren().get(1);
            ImageView img = (ImageView) pane.getChildren().get(2);
            Label titulo = (Label) pane.getChildren().get(3);
            Label genero = (Label) pane.getChildren().get(4);
            Label fecha = (Label) pane.getChildren().get(5);


            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    genero.setText(generos);
                    titulo.setText(pelicula.getTitulo());
                    titulo.autosize();
                    titulo.setStyle("-fx-font-weight: bold");
                    fecha.setText(pelicula.getFecha().toString());
                    descripcion.setText(pelicula.getDescripcion());
                }
            });


            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Image tmp = new Image(pelicula.getImagenPortada());
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                img.setImage(tmp);
                            }
                        });

                    } catch (Exception e) {
                    }
                }
            }).start();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Image tmp = new Image(pelicula.getImagenFondo());
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                imgFondo.setImage(tmp);
                            }
                        });

                    } catch (Exception e) {
                    }
                }
            }).start();
        }
    }
}
