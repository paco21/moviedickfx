package com.moviedick.fx.fxml.enlaces;

import com.moviedick.fx.model.dao.tables.EnlacesDao;
import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.IdiomasDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Enlace;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class UpdateController implements Initializable {
    @FXML
    private TextField enlaces;
    @FXML
    private Button btGuardar;
    @FXML
    private Button btBuscar;
    @FXML
    private Label mensajeInformacion;
    @FXML
    private MenuButton cbGenero;

    @FXML
    private ComboBox<Pelicula> cbPelicula;

    @FXML
    private ComboBox<Idioma> cbIdioma;

    private List<Genero> generos = new ArrayList<>();
    private ObservableList<Pelicula> peliculas;
    private ObservableList<Idioma> idiomas;
    private String[] enlacesList;
    private Enlace enlaceSeleccionado;


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        btGuardar.setDisable(true);
        comboBoxsSetConvert();
        habilitaCampos(false);
        cambiotextEnlaces();
        cargaComboGeneros();
        cargaComboIdiomas();
        cargaComboPeliculas();

    }

    @FXML
    public void buscar(ActionEvent event) {
                  if (!enlaces.getText().equals("")) {
                comprobarEnlaces();
            } else {
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "EL campo enlaces esta vacio");
            }


    }


    private void cambiotextEnlaces() {
        enlaces.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                habilitaCampos(false);
                btGuardar.setDisable(true);
                btBuscar.setVisible(true);
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Los enlaces no se pueden modificar");

            }
        });
    }


    @FXML
    public void guardar(ActionEvent event) {
        obtieneDatos();
        if (EnlacesDao.connect().update(enlaceSeleccionado)) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Se ha modificado");
        } else
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "No se ha modificado, ya existe ese enlace");
        habilitaCampos(false);
        btBuscar.setVisible(true);
        btGuardar.setDisable(true);
    }


    private void cargaComboGeneros() {
        generos = GenerosDao.connect().listAll();
        cbGenero.getItems().clear();
        for (Genero genero : generos
        ) {

            CheckMenuItem item0 = new CheckMenuItem(genero.getNombre());
            item0.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    cargaComboPeliculas();
                }
            });
            cbGenero.getItems().add(item0);
        }

    }

    private void cargaComboIdiomas() {
        idiomas = FXCollections.observableArrayList();
        idiomas.addAll(IdiomasDao.connect().listAll());
        cbIdioma.setItems(idiomas);
        cbIdioma.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbIdioma.getValue() != null && cbPelicula.getValue()!=null) {
                    btGuardar.setDisable(false);
                } else {
                    btGuardar.setDisable(true);
                }
            }
        });
    }

    private void cargaComboPeliculas() {
        List<Genero> tmp = new ArrayList<>();
        for (int i = 0; i < cbGenero.getItems().size(); i++) {
            if (((CheckMenuItem) cbGenero.getItems().get(i)).isSelected()) {
                tmp.add(generos.get(i));
            }
        }
        peliculas = FXCollections.observableArrayList(PeliculasGenerosDao.connect().listAllFilterGenero(tmp));
        cbPelicula.setItems(peliculas);
        cbPelicula.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbPelicula.getValue() != null && cbIdioma.getValue()!=null) {
                    btGuardar.setDisable(false);
                } else {
                    btGuardar.setDisable(true);
                }
            }
        });
    }

    private void comboBoxsSetConvert() {
        cbPelicula.setConverter(new StringConverter<Pelicula>() {
            @Override
            public String toString(Pelicula object) {
                return object.getTitulo();
            }

            @Override
            public Pelicula fromString(String string) {
                return null;
            }
        });
        cbIdioma.setConverter(new StringConverter<Idioma>() {
            @Override
            public String toString(Idioma object) {
                return object.getNombre();
            }

            @Override
            public Idioma fromString(String string) {
                return null;
            }
        });
    }


    private void obtieneDatos() {
        enlaceSeleccionado.setEnlace(enlaces.getText().trim());
        enlaceSeleccionado.setIdioma(cbIdioma.getValue());
        enlaceSeleccionado.setPelicula(cbPelicula.getValue());
    }

    private void limpiaCampos() {
        for (MenuItem menuItem :
                cbGenero.getItems()) {
            ((CheckMenuItem) menuItem).setSelected(false);
            {
            }
        }
    }

    private void habilitaCampos(boolean editable) {
        for (MenuItem menuItem :
                cbGenero.getItems()) {
            menuItem.setStyle("-fx-opacity: 2");
            menuItem.setDisable(!editable);
            {
            }
        }
        cbIdioma.setDisable(!editable);
        cbPelicula.setDisable(!editable);
        if (!editable) {
            for (MenuItem menuItem :
                    cbGenero.getItems()) {
                ((CheckMenuItem) menuItem).setDisable(true);
                {
                }
            }
        }
    }

    private void comprobarEnlaces() {
            Enlace a = new Enlace();
            a.setEnlace(enlaces.getText().trim());
            Enlace enlace = EnlacesDao.connect().get(a);
            if (enlace != null) {
                habilitaCampos(true);
                btBuscar.setVisible(false);
                btGuardar.setDisable(false);
                cbIdioma.setValue(enlace.getIdioma());
                cbPelicula.setValue(enlace.getPelicula());
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Ya existe enlace -> Puedes modificar");
                enlaceSeleccionado=enlace;
            } else {
                if(cbIdioma.getValue()!=null && cbPelicula.getValue()!=null){
                    btGuardar.setDisable(true);
                }else{
                    btGuardar.setDisable(false);
                }
                btBuscar.setVisible(true);
                habilitaCampos(false);
                limpiaCampos();
            }


    }



}
