package com.moviedick.fx.fxml.enlaces;

import com.moviedick.fx.model.dao.tables.*;
import com.moviedick.fx.model.entity.*;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.StringConverter;

import java.lang.reflect.Array;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AltaController implements Initializable {
    @FXML
    private TextArea enlaces;
    @FXML
    private Button btGuardar;
    @FXML
    private Button btBuscar;
    @FXML
    private Label mensajeInformacion;
    @FXML
    private MenuButton cbGenero;

    @FXML
    private ComboBox<Pelicula> cbPelicula;

    @FXML
    private ComboBox<Idioma> cbIdioma;

    private List<Genero> generos = new ArrayList<>();
    private ObservableList<Pelicula> peliculas;
    private ObservableList<Idioma> idiomas;
    private String[] enlacesList;


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        btGuardar.setDisable(true);
        cargaComboGeneros();
        cargaComboPeliculas();
        cargaComboIdiomas();
        comboBoxsSetConvert();
        habilitaCampos(false);
        cambiotextEnlaces();

    }

    @FXML
    public void buscar(ActionEvent event) {
        enlacesList = enlaces.getText().trim().split("\\n");
        if (!comprobarEnlacesRepetidos()) {

            if (!enlaces.getText().equals("")) {
                comprobarEnlaces();
            } else {
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "EL campo enlaces esta vacio");
            }
        }

    }


    private void cambiotextEnlaces() {
        enlaces.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                habilitaCampos(false);
                btGuardar.setDisable(true);
                if (comprobarMaximoEnlaces() < 5) {
                    ;
                    btBuscar.setVisible(true);
                } else {
                    btBuscar.setVisible(false);
                    UtilBanner.mensajeTemporal(mensajeInformacion,
                            "Error,existen mas de 4 saltos de linea");
                }

            }
        });
    }


    @FXML
    public void guardar(ActionEvent event) {

        if (EnlacesDao.connect().add(obtieneDatos())) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Se ha añadido los enlaces");
        } else
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "No se ha podido guardar los enlaces");
        habilitaCampos(false);
        btGuardar.setDisable(true);
    }


    private void cargaComboGeneros() {
        generos = GenerosDao.connect().listAll();
        cbGenero.getItems().clear();
        for (Genero genero : generos
        ) {

            CheckMenuItem item0 = new CheckMenuItem(genero.getNombre());
            item0.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    cargaComboPeliculas();
                }
            });
            cbGenero.getItems().add(item0);
        }

    }

    private void cargaComboIdiomas() {
        idiomas = FXCollections.observableArrayList();
        idiomas.addAll(IdiomasDao.connect().listAll());
        cbIdioma.setItems(idiomas);
        cbIdioma.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbIdioma.getValue() != null && cbPelicula.getValue()!=null) {
                    btGuardar.setDisable(false);
                } else {
                    btGuardar.setDisable(true);
                }
            }
        });
    }

    private void cargaComboPeliculas() {
        List<Genero> tmp = new ArrayList<>();
        for (int i = 0; i < cbGenero.getItems().size(); i++) {
            if (((CheckMenuItem) cbGenero.getItems().get(i)).isSelected()) {
                tmp.add(generos.get(i));
            }
        }
        peliculas = FXCollections.observableArrayList(PeliculasGenerosDao.connect().listAllFilterGenero(tmp));
        cbPelicula.setItems(peliculas);
        cbPelicula.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbPelicula.getValue() != null && cbIdioma.getValue()!=null) {
                    btGuardar.setDisable(false);
                } else {
                    btGuardar.setDisable(true);
                }
            }
        });
    }

    private void comboBoxsSetConvert() {
        cbPelicula.setConverter(new StringConverter<Pelicula>() {
            @Override
            public String toString(Pelicula object) {
                return object.getTitulo();
            }

            @Override
            public Pelicula fromString(String string) {
                return null;
            }
        });
        cbIdioma.setConverter(new StringConverter<Idioma>() {
            @Override
            public String toString(Idioma object) {
                return object.getNombre();
            }

            @Override
            public Idioma fromString(String string) {
                return null;
            }
        });
    }


    private List<Enlace> obtieneDatos() {
        List<Enlace> enlacesTmp = new ArrayList<>();
        for (String enlaceString : enlacesList
        ) {
            Enlace enlace = new Enlace();
            try {
                Thread.sleep(0,1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            enlace.setIdEnlace(enlace.generarId());
            enlace.setEnlace(enlaceString);
            enlace.setIdioma(cbIdioma.getValue());
            enlace.setPelicula(cbPelicula.getValue());
            enlacesTmp.add(enlace);
        }
        return enlacesTmp;
    }

    private void limpiaCampos() {
        for (MenuItem menuItem :
                cbGenero.getItems()) {
            ((CheckMenuItem) menuItem).setSelected(false);
            {
            }
        }
    }

    private void habilitaCampos(boolean editable) {
        for (MenuItem menuItem :
                cbGenero.getItems()) {
            menuItem.setStyle("-fx-opacity: 2");
            menuItem.setDisable(!editable);
            {
            }
        }
        cbIdioma.setDisable(!editable);
        cbPelicula.setDisable(!editable);
        if (!editable) {
            for (MenuItem menuItem :
                    cbGenero.getItems()) {
                ((CheckMenuItem) menuItem).setDisable(true);
                {
                }
            }
        }
    }

    private void comprobarEnlaces() {
        String enlacesNumero = "";
        for (int i = 0; i < enlacesList.length; i++) {
            Enlace a = new Enlace();
            a.setEnlace(enlacesList[i].trim());
            Enlace enlace = EnlacesDao.connect().get(a);
            if (enlace != null) {
                habilitaCampos(false);
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
                enlacesNumero += (i + 1) + " ";
            } else if (enlacesNumero.equals("")) {
                if(cbIdioma.getValue()!=null && cbPelicula.getValue()!=null){
                    btGuardar.setDisable(false);
                }else{
                    btGuardar.setDisable(true);
                }
                btBuscar.setVisible(false);
                habilitaCampos(true);
                limpiaCampos();
            }
        }

        if (!enlacesNumero.equals("")) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Ya existe enlace/es -> El numero de enlace " + enlacesNumero);
        }
    }

    private int comprobarMaximoEnlaces() {
        String enlacesComprobacion = enlaces.getText().trim();
        String separador = "\n";
        int contador = 0;
        while (enlacesComprobacion.indexOf("\n") > -1) {
            enlacesComprobacion = enlacesComprobacion.substring(enlacesComprobacion.indexOf(
                    separador) + separador.length());
            contador++;
        }
        return contador;
    }

    private boolean comprobarEnlacesRepetidos() {
        int h = 0;
        boolean repetido = false;
        while (!repetido && h < enlacesList.length) {
            int j = h + 1;
            while (!repetido && j < enlacesList.length) {
                if (enlacesList[h].equals(enlacesList[j])) {
                    repetido = true;
                    UtilBanner.mensajeTemporal(mensajeInformacion,
                            "Existen enlaces repetidos");
                }
                j++;
            }
            h++;
        }
        return repetido;
    }

}
