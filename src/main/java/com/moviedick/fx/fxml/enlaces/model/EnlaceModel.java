package com.moviedick.fx.fxml.enlaces.model;

import com.moviedick.fx.model.entity.Pelicula;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;

import java.time.format.DateTimeFormatter;

public class EnlaceModel {

    private Pelicula pelicula;
    private final StringProperty tituloProperty;
    private final StringProperty portadaProperty;
    private final StringProperty fondoProperty;
    private final StringProperty descripcionProperty;
    private final StringProperty fechaProperty;
    private final Button btConsulta;
    private final Button btBorra;

    public EnlaceModel() {
        this(null, null, null);
    }

    public EnlaceModel(Pelicula pelicula, Button btConsulta, Button btBorra) {
        this.pelicula = pelicula;
        this.btConsulta = btConsulta;
        this.btBorra = btBorra;
        if (pelicula != null) {
            tituloProperty = new SimpleStringProperty(pelicula.getTitulo());
            portadaProperty = new SimpleStringProperty(pelicula.getImagenPortada());
            fondoProperty = new SimpleStringProperty(pelicula.getImagenFondo());
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            descripcionProperty = new SimpleStringProperty(pelicula.getDescripcion());
            if (pelicula.getFecha()!=null)
            	fechaProperty = new SimpleStringProperty(pelicula.getFecha().format(dtf));
            else
            	fechaProperty = new SimpleStringProperty();
        } else {
            tituloProperty = null;
            portadaProperty = null;
            fondoProperty = null;
            descripcionProperty = null;
            fechaProperty = null;
        }
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public String getTituloProperty() {
        return tituloProperty.get();
    }

    public StringProperty tituloPropertyProperty() {
        return tituloProperty;
    }

    public String getPortadaProperty() {
        return portadaProperty.get();
    }

    public StringProperty portadaPropertyProperty() {
        return portadaProperty;
    }

    public String getFondoProperty() {
        return fondoProperty.get();
    }

    public StringProperty fondoPropertyProperty() {
        return fondoProperty;
    }

    public String getDescripcionProperty() {
        return descripcionProperty.get();
    }

    public StringProperty descripcionPropertyProperty() {
        return descripcionProperty;
    }

    public String getFechaProperty() {
        return fechaProperty.get();
    }

    public StringProperty fechaPropertyProperty() {
        return fechaProperty;
    }

    public Button getBtConsulta() {
        return btConsulta;
    }

    public Button getBtBorra() {
        return btBorra;
    }
}
