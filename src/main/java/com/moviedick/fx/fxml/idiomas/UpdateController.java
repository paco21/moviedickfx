package com.moviedick.fx.fxml.idiomas;

import com.moviedick.fx.fxml.generos.model.GeneroModel;
import com.moviedick.fx.fxml.idiomas.model.IdiomaModel;
import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.IdiomasDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.util.UtilBanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class UpdateController implements Initializable {
    @FXML
    private Label mensajeInformacion;
    @FXML
    private TableView<IdiomaModel> tvListado;
    @FXML
    private TableColumn<IdiomaModel, String> tcNombre;

    @FXML
    private TableColumn<IdiomaModel, Button> tcModificar;


    private List<Idioma> lista=new ArrayList<>();
    private ObservableList<IdiomaModel> listaModel;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
cargaTabla();
    }

    private void cargaTabla() {
        if (lista.size() > 0) {
            listaModel.clear();
            lista.clear();
        }

        listaModel = FXCollections.observableArrayList();
        lista= IdiomasDao.connect().listAll();

        lista.forEach((idioma) -> {
            Button btModificar=new Button("Modificar");
            IdiomaModel model =new IdiomaModel(idioma, btModificar,null);
            btModificar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                   newCuadroDialogo(idioma,model);
                }
            });
            listaModel.add(model);
        });
        tcNombre.setCellValueFactory(cellData -> cellData.getValue().nombrePropertyProperty());

        tcModificar.setCellValueFactory(new PropertyValueFactory<>("btConsulta"));
        tvListado.setItems(listaModel);
    }

    private void newCuadroDialogo(Idioma idioma, IdiomaModel model) {
        TextInputDialog dialogoTextual = new TextInputDialog(idioma.getNombre());
        dialogoTextual.setTitle("Modificar género");
        dialogoTextual.setHeaderText("Pulse aceptar para guardar el cambio(NO se acepta campo en blanco ni el mismo nombre)");
        dialogoTextual.setContentText("Introduzca el nuevo nombre para el género");
        dialogoTextual.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta = dialogoTextual.showAndWait();
        respuesta.ifPresent(respues -> comprobarRespuesta(respues,idioma,model));
    }

    private void comprobarRespuesta(String respuesta, Idioma idioma, IdiomaModel model) {
            if (respuesta.equals(idioma.getNombre()) || respuesta.equals("")) {
                newCuadroDialogo(idioma,model);
            } else {
                idioma.setNombre(respuesta);
                if(IdiomasDao.connect().update(idioma)) {
                    model.setNombreProperty(respuesta);
                }else{
                    UtilBanner.mensajeTemporal(mensajeInformacion,
                            "Ya existe ese idioma");
                }
            }
    }

}
