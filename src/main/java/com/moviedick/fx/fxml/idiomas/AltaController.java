package com.moviedick.fx.fxml.idiomas;

import com.moviedick.fx.model.dao.tables.IdiomasDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AltaController implements Initializable {
    @FXML
    private TextField nombre;
       @FXML
    private Button btGuardar;
    @FXML
    private Button btBuscar;
    @FXML
    private Label mensajeInformacion;
    @FXML
    private FlowPane listaIdiomas;


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        listaIdiomas.setVgap(5.0);
        listaIdiomas.setHgap(5.0);
        btGuardar.setDisable(true);
        for (Idioma idioma: IdiomasDao.connect().listAll()
             ) {
            addTagGenero(idioma);
        }


    }


    @FXML
    public void buscar(ActionEvent event) {

        if (!nombre.getText().equals("")) {
            Idioma idioma = IdiomasDao.connect().get(new Idioma(null,null,nombre.getText()));
            if (idioma != null) {
                muestraDatos(idioma);
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
				UtilBanner.mensajeTemporal(mensajeInformacion,
						"Ya existe ese idioma");
            } else {
                btBuscar.setVisible(false);
                btGuardar.setDisable(false);
                cambioTextNombre();

            }
        } else {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "El campo nombre esta vacio");
        }

    }

    private void cambioTextNombre() {
        nombre.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Vuelva a buscar de nuevo");
            }
        });
    }

    @FXML
    public void guardar(ActionEvent event) {
        Idioma idioma = obtieneDatos();


        if (IdiomasDao.connect().add(idioma) != null) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Se ha añadido el idioma");
            addTagGenero(idioma);

        } else
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "No se ha podido guardar el idioma");
        btGuardar.setDisable(true);
    }




    private void muestraDatos(Idioma idioma) {
        nombre.setText(idioma.getNombre());

        //cbGrupo.getSelectionModel().select(pelicula.getGrupo());
    }

    private Idioma obtieneDatos() {
        Idioma idioma = new Idioma();
        idioma.setIdIdioma(idioma.generarId());
        idioma.setNombre(nombre.getText());
        return idioma;
    }

    private void addTagGenero(Idioma idioma) {
        Label a=new Label("  "+idioma.getNombre());
        a.setStyle("-fx-background-color: #AAA;-fx-background-radius:20;-fx-text-alignment:center");
        a.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                a.setStyle("-fx-background-color: white;-fx-background-radius:20;-fx-text-alignment:center");
            }
        });
        a.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                a.setStyle("-fx-background-color: #AAA;-fx-background-radius:20;-fx-text-alignment:center");
            }
        });

        listaIdiomas.getChildren().add(a);
    }


}
