package com.moviedick.fx.fxml.idiomas.model;

import com.moviedick.fx.model.entity.Idioma;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;

public class IdiomaModel {

    private Idioma idioma;
    private final StringProperty nombreProperty;
    private final Button btConsulta;
    private final Button btBorra;

    public IdiomaModel() {
        this(null, null, null);
    }

    public IdiomaModel(Idioma idioma, Button btConsulta, Button btBorra) {
        this.idioma = idioma;
        this.btConsulta = btConsulta;
        this.btBorra = btBorra;
        if (idioma != null) {
            nombreProperty = new SimpleStringProperty(idioma.getNombre());

        } else {
            nombreProperty = null;
        }
    }

    public void setNombreProperty(String nombreProperty) {
        this.nombreProperty.set(nombreProperty);
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    public Idioma getIdioma() {
        return idioma;
    }

    public String getNombreProperty() {
        return nombreProperty.get();
    }

    public StringProperty nombrePropertyProperty() {
        return nombreProperty;
    }

    public Button getBtConsulta() {
        return btConsulta;
    }

    public Button getBtBorra() {
        return btBorra;
    }
}
