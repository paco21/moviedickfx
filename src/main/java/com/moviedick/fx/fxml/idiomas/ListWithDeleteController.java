package com.moviedick.fx.fxml.idiomas;

import com.moviedick.fx.fxml.generos.model.GeneroModel;
import com.moviedick.fx.fxml.idiomas.model.IdiomaModel;
import com.moviedick.fx.model.dao.tables.EnlacesDao;
import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.IdiomasDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.util.UtilBanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ListWithDeleteController implements Initializable {
    @FXML
    private Label mensajeInformacion;
    @FXML
    private TableView<IdiomaModel> tvListado;
    @FXML
    private TableColumn<IdiomaModel, String> tcNombre;

    @FXML
    private TableColumn<IdiomaModel, Button> tcEliminar;


    private List<Idioma> lista=new ArrayList<>();
    private ObservableList<IdiomaModel> listaModel;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
cargaTabla();
    }

    private void cargaTabla() {
        if (lista.size() > 0) {
            listaModel.clear();
            lista.clear();
        }

        listaModel = FXCollections.observableArrayList();
        lista= IdiomasDao.connect().listAll();

        lista.forEach((idioma) -> {
            Button btEliminar=new Button("Eliminar");
            IdiomaModel model =new IdiomaModel(idioma, null,btEliminar);
            btEliminar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if(IdiomasDao.connect().deleteIdioma(idioma.getIdIdioma())){
                        listaModel.remove(model);
                    }else{
                        UtilBanner.mensajeTemporal(mensajeInformacion,
                                "Error al eliminar el idioma");
                    }

                }
            });
            listaModel.add(model);
        });
        tcNombre.setCellValueFactory(cellData -> cellData.getValue().nombrePropertyProperty());

        tcEliminar.setCellValueFactory(new PropertyValueFactory<>("btBorra"));
        tvListado.setItems(listaModel);
    }


}
