package com.moviedick.fx.fxml.generos;

import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AltaController implements Initializable {
    @FXML
    private TextField nombre;
       @FXML
    private Button btGuardar;
    @FXML
    private Button btBuscar;
    @FXML
    private Label mensajeInformacion;
    @FXML
    private FlowPane listaGeneros;


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        listaGeneros.setVgap(5.0);
        listaGeneros.setHgap(5.0);
        btGuardar.setDisable(true);
        for (Genero genero: GenerosDao.connect().listAll()
             ) {
            addTagGenero(genero);
        }


    }


    @FXML
    public void buscar(ActionEvent event) {

        if (!nombre.getText().equals("")) {
            Genero genero = GenerosDao.connect().get(new Genero(null,null,nombre.getText()));
            if (genero != null) {
                muestraDatos(genero);
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
				UtilBanner.mensajeTemporal(mensajeInformacion,
						"Ya existe ese genero");
            } else {
                btBuscar.setVisible(false);
                btGuardar.setDisable(false);
                cambioTextNombre();

            }
        } else {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "El campo nombre esta vacio");
        }

    }

    private void cambioTextNombre() {
        nombre.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Vuelva a buscar de nuevo");
            }
        });
    }

    @FXML
    public void guardar(ActionEvent event) {
        Genero genero = obtieneDatos();


        if (GenerosDao.connect().add(genero) != null) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Se ha añadido el género");
            addTagGenero(genero);

        } else
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "No se ha podido guardar el género");
        btGuardar.setDisable(true);
    }




    private void muestraDatos(Genero genero) {
        nombre.setText(genero.getNombre());

        //cbGrupo.getSelectionModel().select(pelicula.getGrupo());
    }

    private Genero obtieneDatos() {
        Genero genero = new Genero();
        genero.setIdGenero(genero.generarId());
        genero.setNombre(nombre.getText());
        return genero;
    }

    private void addTagGenero(Genero genero) {
        Label a=new Label("  "+genero.getNombre());
        a.setStyle("-fx-background-color: #AAA;-fx-background-radius:20;-fx-text-alignment:center");
        a.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                a.setStyle("-fx-background-color: white;-fx-background-radius:20;-fx-text-alignment:center");
            }
        });
        a.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                a.setStyle("-fx-background-color: #AAA;-fx-background-radius:20;-fx-text-alignment:center");
            }
        });

        listaGeneros.getChildren().add(a);
    }


}
