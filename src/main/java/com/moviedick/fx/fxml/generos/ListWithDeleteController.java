package com.moviedick.fx.fxml.generos;

import com.moviedick.fx.fxml.generos.model.GeneroModel;
import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.util.UtilBanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ListWithDeleteController implements Initializable {
    @FXML
    private Label mensajeInformacion;
    @FXML
    private TableView<GeneroModel> tvListado;
    @FXML
    private TableColumn<GeneroModel, String> tcNombre;

    @FXML
    private TableColumn<GeneroModel, Button> tcEliminar;


    private List<Genero> lista=new ArrayList<>();
    private ObservableList<GeneroModel> listaModel;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
cargaTabla();
    }

    private void cargaTabla() {
        if (lista.size() > 0) {
            listaModel.clear();
            lista.clear();
        }

        listaModel = FXCollections.observableArrayList();
        lista=GenerosDao.connect().listAll();

        lista.forEach((genero) -> {
            Button btEliminar=new Button("Eliminar");
            GeneroModel model =new GeneroModel(genero, null,btEliminar);
            btEliminar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    if(GenerosDao.connect().deleteGenero(genero.getIdGenero())) {
                        listaModel.remove(model);
                    }else{
                        UtilBanner.mensajeTemporal(mensajeInformacion,
                                "Error al eliminar el genero");
                    }
                }
            });
            listaModel.add(model);
        });
        tcNombre.setCellValueFactory(cellData -> cellData.getValue().nombrePropertyProperty());

        tcEliminar.setCellValueFactory(new PropertyValueFactory<>("btBorra"));
        tvListado.setItems(listaModel);
    }


}
