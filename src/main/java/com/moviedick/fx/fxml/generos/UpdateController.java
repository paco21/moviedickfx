package com.moviedick.fx.fxml.generos;

import com.moviedick.fx.fxml.generos.model.GeneroModel;

import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.util.UtilBanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.StageStyle;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class UpdateController implements Initializable {
    @FXML
    private Label mensajeInformacion;
    @FXML
    private TableView<GeneroModel> tvListado;
    @FXML
    private TableColumn<GeneroModel, String> tcNombre;

    @FXML
    private TableColumn<GeneroModel, Button> tcModificar;


    private List<Genero> lista=new ArrayList<>();
    private ObservableList<GeneroModel> listaModel;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
cargaTabla();
    }

    private void cargaTabla() {
        if (lista.size() > 0) {
            listaModel.clear();
            lista.clear();
        }

        listaModel = FXCollections.observableArrayList();
        lista=GenerosDao.connect().listAll();

        lista.forEach((genero) -> {
            Button btModificar=new Button("Modificar");
            GeneroModel model =new GeneroModel(genero, btModificar,null);
            btModificar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                   newCuadroDialogo(genero,model);
                }
            });
            listaModel.add(model);
        });
        tcNombre.setCellValueFactory(cellData -> cellData.getValue().nombrePropertyProperty());

        tcModificar.setCellValueFactory(new PropertyValueFactory<>("btConsulta"));
        tvListado.setItems(listaModel);
    }

    private void newCuadroDialogo(Genero genero, GeneroModel model) {
        TextInputDialog dialogoTextual = new TextInputDialog(genero.getNombre());
        dialogoTextual.setTitle("Modificar género");
        dialogoTextual.setHeaderText("Pulse aceptar para guardar el cambio(NO se acepta campo en blanco ni el mismo nombre)");
        dialogoTextual.setContentText("Introduzca el nuevo nombre para el género");
        dialogoTextual.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta = dialogoTextual.showAndWait();
        respuesta.ifPresent(respues -> comprobarRespuesta(respues,genero,model));
    }

    private void comprobarRespuesta(String respuesta, Genero genero, GeneroModel model) {
            if (respuesta.equals(genero.getNombre()) || respuesta.equals("")) {
                newCuadroDialogo(genero,model);
            } else {
                genero.setNombre(respuesta);
                if(GenerosDao.connect().update(genero)){
                    model.setNombreProperty(respuesta);
                }else{
                    UtilBanner.mensajeTemporal(mensajeInformacion,
                            "Ya existe ese genero");
                }

            }
    }

}
