package com.moviedick.fx.fxml.generos.model;

import com.moviedick.fx.model.entity.Genero;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;

public class GeneroModel {

    private Genero genero;
    private final StringProperty nombreProperty;
    private final Button btConsulta;
    private final Button btBorra;

    public GeneroModel() {
        this(null, null, null);
    }

    public GeneroModel(Genero genero, Button btConsulta, Button btBorra) {
        this.genero = genero;
        this.btConsulta = btConsulta;
        this.btBorra = btBorra;
        if (genero != null) {
            nombreProperty = new SimpleStringProperty(genero.getNombre());

        } else {
            nombreProperty = null;
        }
    }

    public void setNombreProperty(String nombreProperty) {
        this.nombreProperty.set(nombreProperty);
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Genero getGenero() {
        return genero;
    }

    public String getNombreProperty() {
        return nombreProperty.get();
    }

    public StringProperty nombrePropertyProperty() {
        return nombreProperty;
    }

    public Button getBtConsulta() {
        return btConsulta;
    }

    public Button getBtBorra() {
        return btBorra;
    }
}
