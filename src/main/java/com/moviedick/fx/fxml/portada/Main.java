package com.moviedick.fx.fxml.portada;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        try {
            Parent screen0 = FXMLLoader.load(getClass().getResource("./Screen0.fxml"));
            Scene scene = new Scene(screen0, 800, 800);
            primaryStage.setTitle("MovieDick");
            primaryStage.setScene(scene);
            primaryStage.setMaximized(true);
            primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
