package com.moviedick.fx.fxml.portada;

import com.moviedick.LoadFilm;
import com.moviedick.OpenBrowser;
import com.moviedick.fx.fxml.portada.utilidades.MainViewModel;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class NavigationDrawerController implements Initializable {
    @FXML
    private Label add;

    @FXML
    private Label labelError;

    @FXML
    private TextField password;

    @FXML
    private Label update;

    @FXML
    private Label listWithDelete;

    @FXML
    private AnchorPane paneMenuAdmin;

    @FXML
    private FlowPane paneSubMenu;

    private ArrayList<Label> labels = new ArrayList<>();
    private ArrayList<String> entidades = new ArrayList<>(Arrays.asList("película", "idioma", "enlace", "género"));
    private String[] funciones = {"Añadir", "Modificar", "Listar con Borrar"};
    private String[] directorio = {"peliculas", "idiomas", "enlaces", "generos"};
    private String[] fxml = {"Alta.fxml", "Update.fxml", "ListWithDelete.fxml"};

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        paneMenuAdmin.setVisible(false);
        paneSubMenu.setVisible(false);
        labels.add(add);
        labels.add(update);
        labels.add(listWithDelete);
        controlBotonesSubMenuAdmin();
    }


    @FXML
    private void menuAdmin(MouseEvent mouseEvent) {
        String value = "";
        try {
            value = getEncrypt(password.getText());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if (value.equals("0DPiKuNIrrVmD8IUCuw1hQxNqZc=")) {
            paneMenuAdmin.setVisible(true);
            labelError.setVisible(false);
        } else {
            labelError.setVisible(true);
        }

    }

    @FXML
    private void onMouseExitZonaAdmin(MouseEvent mouseEvent) {
        labelError.setVisible(false);
    }

    @FXML
    private void onMouseExitPaneMenuAdmin(MouseEvent mouseEvent) {
        paneMenuAdmin.setVisible(false);
        paneSubMenu.setVisible(false);

    }

    @FXML
    private void onMouseExitPaneSubMenuAdmin(MouseEvent mouseEvent) {
        paneSubMenu.setVisible(false);
    }

    @FXML
    private void subMenuAdminPelicula(MouseEvent mouseEvent) {
        paneSubMenu.setVisible(true);
        cambiarSubMenu(0);

    }

    @FXML
    private void subMenuAdminGenero(MouseEvent mouseEvent) {
        paneSubMenu.setVisible(true);
        cambiarSubMenu(3);
    }

    @FXML
    private void subMenuAdminEnlace(MouseEvent mouseEvent) {

        paneSubMenu.setVisible(true);
        cambiarSubMenu(2);
    }

    @FXML
    private void subMenuAdminIdioma(MouseEvent mouseEvent) {

        paneSubMenu.setVisible(true);
        cambiarSubMenu(1);
    }

    private void controlBotonesSubMenuAdmin() {
        for (int i = 0; i < labels.size(); i++) {
            int finalI = i;
            labels.get(i).setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    String[] opcion = labels.get(finalI).getText().split(" ");
                    cargaOpcionMenu("../" + directorio[entidades.indexOf(opcion[opcion.length - 1])] + "/" + fxml[finalI]);
                }
            });
        }


    }

    private void cambiarSubMenu(int h) {

        int i = 0;
        for (Label label : labels
        ) {
            label.setText(funciones[i++] + " " + entidades.get(h));
        }
    }

    private String getEncrypt(String input) throws NoSuchAlgorithmException {
        byte[] raw_data = input.getBytes(StandardCharsets.UTF_8);
        MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
        mdTemp.update(raw_data);
        byte[] bytes = mdTemp.digest();
        return new String(Base64.getEncoder().encode(bytes));
    }


    @FXML
    private void onMouseClickDiscovery(MouseEvent mouseEvent) {
        PeliculasDao.connect().sortConsulta(PeliculasDao.connect().SORT_FECHA_DESC);
        LoadFilm discoveryFilm=new LoadFilm(PeliculasDao.connect().listAll(),0);
        PeliculasDao.connect().clearSortConsulta();
        discoveryFilm.cargarItems();

    }

    @FXML
    private void onMouseClickFilms(MouseEvent mouseEvent) {
        PeliculasDao.connect().sortConsulta(PeliculasDao.connect().SORT_IDPELICULA_DESC);
        LoadFilm discoveryFilm=new LoadFilm(PeliculasDao.connect().listAll(),0);
        PeliculasDao.connect().clearSortConsulta();
        discoveryFilm.cargarItems();

    }

    @FXML
    private void onMouseClickAddFilms(MouseEvent mouseEvent) {
        cargaOpcionMenu("../" + directorio[0] + "/" + fxml[0]);
    }

    @FXML
    private void onMouseClickTwitter(MouseEvent mouseEvent) {
        OpenBrowser navegador=new OpenBrowser();
        try {
            navegador.abrirNavegadorPorDefecto("https://twitter.com/xDedeCo_app");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void cargaOpcionMenu(String fxml) {
        FlowPane container = MainViewModel.MainViewModel().getContainer();
        try {
            container.setAlignment(Pos.CENTER);
            container.setStyle("-fx-background-color: #181821");
            //container.getStyleClass().add("container-select-item");
            //container.getStylesheets().add(getClass().getResource("../styles/subMenuAdmin.css").toString());
            AnchorPane pane = FXMLLoader.load(getClass().getResource(fxml));
            container.getChildren().clear();
            container.getChildren().add(pane);
        } catch (IOException e) {
        }
    }


}
