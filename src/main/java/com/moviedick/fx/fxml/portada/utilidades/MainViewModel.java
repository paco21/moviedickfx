package com.moviedick.fx.fxml.portada.utilidades;

import com.moviedick.fx.fxml.portada.Main;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

public class MainViewModel {
    private static MainViewModel mainViewModel;
    private FlowPane container;

    public static MainViewModel MainViewModel() {
        if(mainViewModel==null){
            mainViewModel=new MainViewModel();
        }
        return mainViewModel;
    }

    public FlowPane getContainer() {
        return container;
    }

    public void setContainer(FlowPane container) {
        this.container = container;
    }
}
