package com.moviedick.fx.fxml.portada;

import com.jfoenix.controls.JFXScrollPane;
import com.moviedick.LoadFilm;
import com.moviedick.fx.fxml.portada.utilidades.MainViewModel;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 */
public class Controller implements Initializable {
    @FXML
    private ScrollPane scrollPane;

    @FXML
    private FlowPane paneMenuNavigationDrawer;
    @FXML
    public FlowPane container;

    @FXML
    private AnchorPane paneMenuToolbar;


    @FXML
    private VBox paneMainContainer;
    @FXML
    private GridPane paneToolBar;
    @FXML
    private GridPane paneBottomBar;
    @FXML
    private Pane paneContentArea;
    @FXML
    private Pane paneEffectDisable;

    private Main main;
    //private  JFXMasonryPane masonryPane;

    @FXML
    private TextField filtroTitulo;
        /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
               paneEffectDisable.setVisible(false);
        MainViewModel.MainViewModel().setContainer(container);
        try {
            //Barra navegacion izquierda
            FlowPane menuNavigationDrawer = FXMLLoader.load(getClass().getResource("./MenuNavigationDrawer.fxml"));
            paneMenuNavigationDrawer.getChildren().add(menuNavigationDrawer);

            //Rellenar contenedor
            scrollPane.setFitToWidth(true);
            PeliculasDao.connect().sortConsulta(PeliculasDao.connect().SORT_FECHA_DESC);
            LoadFilm discoveryFilm=new LoadFilm(PeliculasDao.connect().listAll(),0);
            PeliculasDao.connect().clearSortConsulta();
            discoveryFilm.cargarItems();

            //Barra nevagacion derecha
            VBox menuToolbar = FXMLLoader.load(getClass().getResource("./MenuToolbar.fxml"));
            paneMenuToolbar.getChildren().add(menuToolbar);
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        paneMenuNavigationDrawer.setVisible(false);
        paneMenuToolbar.setVisible(false);
        Platform.runLater(() -> scrollPane.requestLayout());
         JFXScrollPane.smoothScrolling(scrollPane);

        cambioTextTitulo();
    }



    @FXML
    private void onMouseExitedPaneNavigationDrawer(MouseEvent event) {
        paneMenuNavigationDrawer.setVisible(false);
        paneEffectDisable.setVisible(false);
    }

    @FXML
    private void onMouseExitedPaneToolbarMenu(MouseEvent event) {
        paneMenuToolbar.setVisible(false);
        paneEffectDisable.setVisible(false);
    }

    @FXML
    private void onMouseClickedMenuNavigationDrawer(MouseEvent event) {
        paneMenuNavigationDrawer.setVisible(!paneMenuNavigationDrawer.isVisible());
        paneMenuToolbar.setVisible(false);
        paneEffectDisable.setVisible(true);
    }

    @FXML
    private void onMouseClickedMenuToolbar(MouseEvent event) {
        paneMenuToolbar.setVisible(!paneMenuToolbar.isVisible());
        paneMenuNavigationDrawer.setVisible(false);
        paneEffectDisable.setVisible(true);
    }

    private void cambioTextTitulo() {
        filtroTitulo.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
               
                        LoadFilm discoveryFilm=new LoadFilm(PeliculasDao.connect().findByTitulo(filtroTitulo.getText()),0);
                        discoveryFilm.cargarItems();

            }
        });

    }


}
