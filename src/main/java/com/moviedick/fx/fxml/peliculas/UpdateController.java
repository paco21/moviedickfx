package com.moviedick.fx.fxml.peliculas;

import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.model.entity.PeliculaGenero;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class UpdateController implements Initializable {
    @FXML
    private TextField titulo;
    @FXML
    private TextField fondo;
    @FXML
    private DatePicker fecha;
    @FXML
    private TextField portada;
    @FXML
    private TextArea descripcion;
    @FXML
    private Button btGuardar;
    @FXML
    private Button btBuscar;
    @FXML
    private Label mensajeInformacion;
    @FXML
    private MenuButton cbGenero;
    @FXML
    private FlowPane listaPeliculas;

    private List<Genero> generos = new ArrayList<>();

    private Pelicula pelicula;


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        listaPeliculas.setVgap(5.0);
        listaPeliculas.setHgap(5.0);
        btGuardar.setDisable(true);
        cargaComboGeneros();
        habilitaCampos(false);
        cambioTextTituloAndFecha(false);
    }

    @FXML
    public void buscar(ActionEvent event) {

        if (!titulo.getText().equals("") && fecha.getValue() != null) {
            pelicula = PeliculasDao.connect().get(new Pelicula(titulo.getText(), fecha.getValue()));
            if (pelicula != null) {
                habilitaCampos(true);
                muestraDatos(pelicula);
                btBuscar.setVisible(true);
                btGuardar.setDisable(false);
				UtilBanner.mensajeTemporal(mensajeInformacion,
						"Pelicula encontrada, puedes modificarla");
                cambioTextTituloAndFecha(true);
            } else {
                btBuscar.setVisible(true);
                habilitaCampos(false);
                limpiaCampos();
                btGuardar.setDisable(true);

                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Pelicula no encontrada");
            }
        } else {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "El campo titulo o fecha esta vacio");
        }

    }

    private void cambioTextTituloAndFecha(boolean mensaje) {
        titulo.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                if(mensaje) {
                    UtilBanner.mensajeTemporal(mensajeInformacion,
                            "Si guardas editarás la pelicula");
                }
                listaPeliculas.getChildren().clear();
                for (Pelicula pelicula: PeliculasDao.connect().findByTitulo(titulo.getText())) {
                    addTagPelicula(pelicula);
                }
            }
        });

        fecha.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                if(mensaje) {
                    UtilBanner.mensajeTemporal(mensajeInformacion,
                            "Si guardas editaras la pelicula");
                }
            }
        });
    }

    @FXML
    public void guardar(ActionEvent event) {
        pelicula = obtieneDatos();


        if (PeliculasDao.connect().update(pelicula)) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Se ha modificado la pelicula, el genero no");

            PeliculasGenerosDao.connect().deleteByIdPelicula(pelicula.getIdPelicula());

            for (MenuItem menuItem :
                    cbGenero.getItems()) {
                    if (((CheckMenuItem) menuItem).isSelected()) {
					PeliculaGenero peliculaGenero=new PeliculaGenero();

					peliculaGenero.setGenero(generos.get(cbGenero.getItems().indexOf(menuItem)));
					peliculaGenero.setPelicula(pelicula);
                    if (PeliculasGenerosDao.connect().add(peliculaGenero)!=null) {
                        UtilBanner.mensajeTemporal(mensajeInformacion,
                                "Se ha modifcado la pelicula y el genero");
                    }
                    // System.out.println(generos.get(cbGenero.getItems().indexOf(menuItem)));
                }
            }


        } else
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "No se ha podido modificar la pelicula, quizas ya exista ese titulo con esa fecha");
        habilitaCampos(false);
        btGuardar.setDisable(true);
    }


    private void cargaComboGeneros() {
        generos = GenerosDao.connect().listAll();
        cbGenero.getItems().clear();
        for (Genero genero : generos
        ) {

            CheckMenuItem item0 = new CheckMenuItem(genero.getNombre());
            cbGenero.getItems().add(item0);
        }

    }

    private void muestraDatos(Pelicula pelicula) {
        titulo.setText(pelicula.getTitulo());
        descripcion.setText(pelicula.getDescripcion());
        portada.setText(pelicula.getImagenPortada());
        fondo.setText(pelicula.getImagenFondo());
        fecha.setValue(pelicula.getFecha());
        pelicula.setPeliculasGeneros(PeliculasGenerosDao.connect().list(new PeliculaGenero(pelicula),null));
        for (int i = 0; i <generos.size(); i++) {
            for (int j = 0; j <pelicula.getPeliculasGeneros().size() ; j++) {
                if(generos.get(i).getNombre().equals(pelicula.getPeliculasGeneros().get(j).getGenero().getNombre())) {
                    ((CheckMenuItem) cbGenero.getItems().get(i)).setSelected(true);

                }}
        }
        //cbGrupo.getSelectionModel().select(pelicula.getGrupo());
    }

    private Pelicula obtieneDatos() {
        pelicula.setTitulo(titulo.getText());
        pelicula.setDescripcion(descripcion.getText());
        pelicula.setImagenPortada(portada.getText());
        pelicula.setImagenFondo(fondo.getText());
        pelicula.setFecha(fecha.getValue());
        return pelicula;
    }

    private void limpiaCampos() {
        descripcion.setText("");
        portada.setText("");
        fondo.setText("");
		for (MenuItem menuItem :
				cbGenero.getItems()) {
			 ((CheckMenuItem) menuItem).setSelected(false);{
			}
		}
    }

    private void habilitaCampos(boolean editable) {
        descripcion.setEditable(editable);
        fecha.setEditable(editable);
        fondo.setEditable(editable);
        portada.setEditable(editable);
		for (MenuItem menuItem :
				cbGenero.getItems()) {
			menuItem.setStyle("-fx-opacity: 2");
			menuItem.setDisable(!editable);{
			}
		}
        if(!editable) {
            for (MenuItem menuItem :
                    cbGenero.getItems()) {
                ((CheckMenuItem) menuItem).setDisable(true);
                {
                }
            }
        }
       // cbGenero.setDisable(!editable);
    }
    private void addTagPelicula(Pelicula pelicula) {
        Label a=new Label("  "+pelicula.getTitulo());
        a.setStyle("-fx-background-color: #AAA;-fx-background-radius:20;-fx-text-alignment:center");
        a.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                a.setStyle("-fx-background-color: white;-fx-background-radius:20;-fx-text-alignment:center");
            }
        });
        a.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                a.setStyle("-fx-background-color: #AAA;-fx-background-radius:20;-fx-text-alignment:center");
            }
        });
        a.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                muestraDatos(pelicula);
                buscar(null);
            }
        });

        listaPeliculas.getChildren().add(a);
    }

}
