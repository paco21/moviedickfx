package com.moviedick.fx.fxml.peliculas;

import com.moviedick.fx.fxml.peliculas.model.PeliculaModel;
import com.moviedick.fx.model.dao.tables.EnlacesDao;
import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.model.entity.PeliculaGenero;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ListWithDeleteController implements Initializable {
    @FXML
    private Label mensajeInformacion;
    @FXML
    private TableView<PeliculaModel> tvListado;
    @FXML
    private TableColumn<PeliculaModel, String> tcTitulo;
    @FXML
    private TableColumn<PeliculaModel, String> tcPortada;
    @FXML
    private TableColumn<PeliculaModel, String> tcFondo;
    @FXML
    private TableColumn<PeliculaModel, String> tcDescripcion;
    @FXML
    private TableColumn<PeliculaModel, String> tcFecha;
    @FXML
    private TableColumn<PeliculaModel, Button> tcEliminar;
    @FXML
    private MenuButton ckbGenero;
    @FXML
    private TextField filtroTitulo;

    private List<Pelicula> lista;
    private ObservableList<PeliculaModel> listaModel;
    private List<Genero> generos = new ArrayList<>();
    private List<Genero> generosSeleccionados = new ArrayList<>();

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        lista = new ArrayList<>();
        listaModel = FXCollections.observableArrayList();
        cargaComboGrupos();
        cambioTextTitulo();
    }


    private void seleccionGrupoCheck() {
        generosSeleccionados.clear();
        for (int i = 0; i < generos.size(); i++) {
            if (((CheckMenuItem) ckbGenero.getItems().get(i)).isSelected()) {
                generosSeleccionados.add(generos.get(i));
            }
        }
        cargaModel();

    }

    private void cargaComboGrupos() {
        generos = GenerosDao.connect().listAll();
        ckbGenero.getItems().clear();
        for (Genero genero : generos
        ) {

            CheckMenuItem item0 = new CheckMenuItem(genero.getNombre());
            item0.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    seleccionGrupoCheck();
                }
            });
            ckbGenero.getItems().add(item0);
        }

    }


    private void cargaModel() {
        if (lista.size() > 0) {
            listaModel.clear();
            lista.clear();
        }
        String texto = "";
        listaModel = FXCollections.observableArrayList();
        for (Genero genero : generosSeleccionados
        ) {
            texto += " " + genero.getNombre();
        }
        for (Pelicula pelicula : PeliculasGenerosDao.connect().listAllFilterGenero(generosSeleccionados)) {
            lista.add(pelicula);
        }
        if (generosSeleccionados.size() == 0) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Filtrando por todas las películas");
        } else {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Filtrando por" + texto);
        }
        cargaTabla();
    }
    private void cargaTabla(){
        lista.forEach((pelicula) -> {
            Button btEliminar = new Button("Eliminar");
            PeliculaModel model = new PeliculaModel(pelicula, null, btEliminar);
            btEliminar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (PeliculasDao.connect().deletePelicula(pelicula.getIdPelicula())) {
                        listaModel.remove(model);
                    } else {
                        UtilBanner.mensajeTemporal(mensajeInformacion,
                                "Error al eliminar la pelicula");
                    }
                    //tvListado.refresh();
                }
            });
            listaModel.add(model);
        });
        tcTitulo.setCellValueFactory(cellData -> cellData.getValue().tituloPropertyProperty());
        tcPortada.setCellValueFactory(cellData -> cellData.getValue().portadaPropertyProperty());
        tcFondo.setCellValueFactory(cellData -> cellData.getValue().fondoPropertyProperty());
        tcDescripcion.setCellValueFactory(cellData -> cellData.getValue().descripcionPropertyProperty());
        tcFecha.setCellValueFactory(cellData -> cellData.getValue().fechaPropertyProperty());
        tcEliminar.setCellValueFactory(new PropertyValueFactory<>("btBorra"));
        tvListado.setItems(listaModel);
    }

    private void cambioTextTitulo() {
        filtroTitulo.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                if (lista.size() > 0) {
                    listaModel.clear();
                    lista.clear();
                }
                for (Pelicula pelicula : PeliculasDao.connect().findByTitulo(filtroTitulo.getText())) {
                    lista.add(pelicula);
                }

                cargaTabla();
            }
        });

    }
}
