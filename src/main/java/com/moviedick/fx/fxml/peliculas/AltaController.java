package com.moviedick.fx.fxml.peliculas;

import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.model.entity.PeliculaGenero;
import com.moviedick.fx.util.UtilBanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AltaController implements Initializable {
    @FXML
    private TextField titulo;
    @FXML
    private TextField fondo;
    @FXML
    private DatePicker fecha;
    @FXML
    private TextField portada;
    @FXML
    private TextArea descripcion;
    @FXML
    private Button btGuardar;
    @FXML
    private Button btBuscar;
    @FXML
    private Label mensajeInformacion;
    @FXML
    private MenuButton cbGenero;

    private List<Genero> generos = new ArrayList<>();


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

        btGuardar.setDisable(true);
        cargaComboGeneros();
        habilitaCampos(false);

    }

    @FXML
    public void buscar(ActionEvent event) {

        if (!titulo.getText().equals("") && fecha.getValue() != null) {
            Pelicula pelicula = PeliculasDao.connect().get(new Pelicula(titulo.getText(), fecha.getValue()));
            if (pelicula != null) {
                habilitaCampos(false);
                muestraDatos(pelicula);
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
				UtilBanner.mensajeTemporal(mensajeInformacion,
						"Ya existe esa pelicula");
            } else {
                btBuscar.setVisible(false);
                habilitaCampos(true);
                limpiaCampos();
                btGuardar.setDisable(false);
                cambioTextTituloAndFecha();

            }
        } else {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "El campo titulo o fecha esta vacio");
        }

    }

    private void cambioTextTituloAndFecha() {
        titulo.textProperty().addListener(new ChangeListener<String>() {
            public void changed(final ObservableValue<? extends String> observableValue, final String oldValue,
                                final String newValue) {
                habilitaCampos(false);
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Vuelva a buscar de nuevo");
            }
        });

        fecha.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                habilitaCampos(false);
                btBuscar.setVisible(true);
                btGuardar.setDisable(true);
                UtilBanner.mensajeTemporal(mensajeInformacion,
                        "Vuelva a buscar de nuevo");
            }
        });
    }

    @FXML
    public void guardar(ActionEvent event) {
        Pelicula pelicula = obtieneDatos();


        if (PeliculasDao.connect().add(pelicula) != null) {
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "Se ha añadido la pelicula");
            for (MenuItem menuItem :
                    cbGenero.getItems()) {
                if (((CheckMenuItem) menuItem).isSelected()) {
					PeliculaGenero peliculaGenero=new PeliculaGenero();
					peliculaGenero.setGenero(generos.get(cbGenero.getItems().indexOf(menuItem)));
					peliculaGenero.setPelicula(pelicula);
                    if (PeliculasGenerosDao.connect().add(peliculaGenero) != null) {
                        UtilBanner.mensajeTemporal(mensajeInformacion,
                                "Se ha añadido la pelicula y el genero");
                    }
                    // System.out.println(generos.get(cbGenero.getItems().indexOf(menuItem)));
                }
            }


        } else
            UtilBanner.mensajeTemporal(mensajeInformacion,
                    "No se ha podido guardar la pelicula");
        habilitaCampos(false);
        btGuardar.setDisable(true);
    }


    private void cargaComboGeneros() {
        generos = GenerosDao.connect().listAll();
        cbGenero.getItems().clear();
        for (Genero genero : generos
        ) {

            CheckMenuItem item0 = new CheckMenuItem(genero.getNombre());
            cbGenero.getItems().add(item0);
        }

    }

    private void muestraDatos(Pelicula pelicula) {
        titulo.setText(pelicula.getTitulo());
        descripcion.setText(pelicula.getDescripcion());
        portada.setText(pelicula.getImagenPortada());
        fondo.setText(pelicula.getImagenFondo());
        fecha.setValue(pelicula.getFecha());
        pelicula.setPeliculasGeneros(PeliculasGenerosDao.connect().list(new PeliculaGenero(pelicula),null));
        for (int i = 0; i <generos.size(); i++) {
            for (int j = 0; j <pelicula.getPeliculasGeneros().size() ; j++) {
                if(generos.get(i).getNombre().equals(pelicula.getPeliculasGeneros().get(j).getGenero().getNombre())) {
                    ((CheckMenuItem) cbGenero.getItems().get(i)).setSelected(true);

                }}
        }
        //cbGrupo.getSelectionModel().select(pelicula.getGrupo());
    }

    private Pelicula obtieneDatos() {
        Pelicula pelicula = new Pelicula();
        pelicula.setIdPelicula(pelicula.generarId());
        pelicula.setTitulo(titulo.getText());
        pelicula.setDescripcion(descripcion.getText());
        pelicula.setImagenPortada(portada.getText());
        pelicula.setImagenFondo(fondo.getText());
        pelicula.setFecha(fecha.getValue());
        return pelicula;
    }

    private void limpiaCampos() {
        descripcion.setText("");
        portada.setText("");
        fondo.setText("");
		for (MenuItem menuItem :
				cbGenero.getItems()) {
			 ((CheckMenuItem) menuItem).setSelected(false);{
			}
		}
    }

    private void habilitaCampos(boolean editable) {
        descripcion.setEditable(editable);
        fecha.setEditable(editable);
        fondo.setEditable(editable);
        portada.setEditable(editable);
		for (MenuItem menuItem :
				cbGenero.getItems()) {
			menuItem.setStyle("-fx-opacity: 2");
			menuItem.setDisable(!editable);{
			}
		}
		if(!editable) {
            for (MenuItem menuItem :
                    cbGenero.getItems()) {
                ((CheckMenuItem) menuItem).setDisable(true);
                {
                }
            }
        }
       // cbGenero.setDisable(!editable);
    }

}
