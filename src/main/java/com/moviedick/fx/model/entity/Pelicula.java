package com.moviedick.fx.model.entity;

import java.time.LocalDate;
import java.util.List;

public class Pelicula extends EntityImp {
    private Integer id;
    private String idPelicula;
    private String titulo;
    private String imagenPortada;
    private String imagenFondo;
    private String descripcion;
    private LocalDate fecha;

    private List<PeliculaGenero> peliculasGeneros;
    private List<Enlace> enlaces;
    private List<Genero> generos;

    public Pelicula() {
    }

    public List<Genero> getGeneros() {
        return generos;
    }

    public void setGeneros(List<Genero> generos) {
        this.generos = generos;
    }

    public Pelicula(Integer id) {
        this.id = id;
    }

    public Pelicula(String idPelicula) {
        this.idPelicula = idPelicula;
    }

    public Pelicula(String titulo, LocalDate fecha) {
        this.titulo = titulo;
        this.fecha = fecha;
    }

    public Pelicula(String idPelicula, String titulo, String imagenPortada, String imagenFondo, String descripcion, LocalDate fecha) {
        this.idPelicula = idPelicula;
        this.titulo = titulo;
        this.imagenPortada = imagenPortada;
        this.imagenFondo = imagenFondo;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(String idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getImagenPortada() {
        return imagenPortada;
    }

    public void setImagenPortada(String imagenPortada) {
        this.imagenPortada = imagenPortada;
    }

    public String getImagenFondo() {
        return imagenFondo;
    }

    public void setImagenFondo(String imagenFondo) {
        this.imagenFondo = imagenFondo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }


    public List<PeliculaGenero> getPeliculasGeneros() {
        return peliculasGeneros;
    }

    public void setPeliculasGeneros(List<PeliculaGenero> peliculasGeneros) {
        this.peliculasGeneros = peliculasGeneros;
    }

    public List<Enlace> getEnlaces() {
        return enlaces;
    }

    public void setEnlaces(List<Enlace> enlaces) {
        this.enlaces = enlaces;
    }

    @Override
    public String toString() {
        return "Pelicula{" +
                "id=" + id +
                ", idPelicula='" + idPelicula + '\'' +
                ", titulo='" + titulo + '\'' +
                ", imagenPortada='" + imagenPortada + '\'' +
                ", imagenFondo='" + imagenFondo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", fecha=" + fecha +
                ", peliculasGeneros=" + peliculasGeneros +
                ", enlaces=" + enlaces +
                '}';
    }
}
