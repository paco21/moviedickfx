package com.moviedick.fx.model.entity;

public class PeliculaGenero extends EntityImp {
    private Integer id;
    private Pelicula pelicula;
    private Genero genero;

    public PeliculaGenero() {
    }

    public PeliculaGenero(Integer id) {
        this.id = id;
    }

    public PeliculaGenero(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public PeliculaGenero(Pelicula pelicula, Genero genero) {
        this.pelicula = pelicula;
        this.genero = genero;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "PeliculaGenero{" +
                "id=" + id +
                ", pelicula=" + pelicula +
                ", genero=" + genero +
                '}';
    }
}
