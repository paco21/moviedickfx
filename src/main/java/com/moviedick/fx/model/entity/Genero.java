package com.moviedick.fx.model.entity;

import java.util.List;

public class Genero extends EntityImp {
    private Integer id;
    private String idGenero;
    private String nombre;
    private List<PeliculaGenero> peliculasGeneros;

    public Genero() {
    }

    public Genero(String idGenero) {
        this.idGenero = idGenero;
    }

    public Genero(Integer id) {
        this.id = id;
    }

    public Genero(String idGenero, String nombre) {
        this.idGenero = idGenero;
        this.nombre = nombre;
    }

    public Genero(Integer id, String idGenero, String nombre) {
        this.id = id;
        this.idGenero = idGenero;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(String idGenero) {
        this.idGenero = idGenero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<PeliculaGenero> getPeliculasGeneros() {
        return peliculasGeneros;
    }

    public void setPeliculasGeneros(List<PeliculaGenero> peliculasGeneros) {
        this.peliculasGeneros = peliculasGeneros;
    }

    @Override
    public String toString() {
        return "Genero{" +
                "id=" + id +
                ", idGenero='" + idGenero + '\'' +
                ", nombre='" + nombre + '\'' +
                ", peliculasGeneros=" + peliculasGeneros +
                '}';
    }
}
