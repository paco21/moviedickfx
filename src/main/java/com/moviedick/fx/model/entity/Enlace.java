package com.moviedick.fx.model.entity;

public class Enlace extends EntityImp {
    private Integer id;

    private String idEnlace;
    private Pelicula pelicula;
    private String enlace;
    private Idioma idioma;

    public Enlace() {
    }

    public Enlace(Integer id) {
        this.id = id;
    }

    public Enlace(String idEnlace) {
        this.idEnlace = idEnlace;
    }

    public Enlace(String idEnlace, Pelicula pelicula, String enlace, Idioma idioma) {
        this.idEnlace = idEnlace;
        this.pelicula = pelicula;
        this.enlace = enlace;
        this.idioma = idioma;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdEnlace() {
        return idEnlace;
    }

    public void setIdEnlace(String idEnlace) {
        this.idEnlace = idEnlace;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public Idioma getIdioma() {
        return idioma;
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "Enlace{" +
                "id=" + id +
                ", idEnlace='" + idEnlace + '\'' +
                ", pelicula=" + pelicula +
                ", enlace='" + enlace + '\'' +
                ", idioma=" + idioma +
                '}';
    }
}
