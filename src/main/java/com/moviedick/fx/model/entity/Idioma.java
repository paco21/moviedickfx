package com.moviedick.fx.model.entity;

import java.util.List;

public class Idioma extends EntityImp {
    private Integer id;
    private String idIdioma;
    private String nombre;
    private List<Enlace> enlaces;

    public Idioma() {
    }

    public Idioma(Integer id, String idIdioma, String nombre) {
        this.id = id;
        this.idIdioma = idIdioma;
        this.nombre = nombre;
    }

    public Idioma(Integer id) {
        this.id = id;
    }

    public Idioma(String idIdioma) {
        this.idIdioma = idIdioma;
    }

    public Idioma(String idIdioma, String nombre) {
        this.idIdioma = idIdioma;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdIdioma() {
        return idIdioma;
    }

    public void setIdIdioma(String idIdioma) {
        this.idIdioma = idIdioma;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Enlace> getEnlaces() {
        return enlaces;
    }

    public void setEnlaces(List<Enlace> enlaces) {
        this.enlaces = enlaces;
    }

    @Override
    public String toString() {
        return "Idioma{" +
                "id=" + id +
                ", idIdioma='" + idIdioma + '\'' +
                ", nombre='" + nombre + '\'' +
                ", enlaces=" + enlaces +
                '}';
    }
}
