package com.moviedick.fx.model.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

abstract class EntityImp implements Entity {
    @Override
    public String generarId() {
        String id = "";
        id = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        id += LocalTime.now().format(DateTimeFormatter.ofPattern("-HmsN"));
        if(id.length()>25){
        id=id.substring(0,25);
        }else{
            for (int i = 0; i <25-id.length() ; i++) {
                id+="0";
            }
        }
        return id;
    }
}
