package com.moviedick.fx.model.dao;


import java.sql.*;

public class ConectaBD {

    private static ConectaBD conectaBD;
    private Connection connection;
    private boolean error;
    private Statement stmtUpdate;

//	final static int DB_VERSION = 1;
//	final static String DB_URL = "castelarlamp.duckdns.org";
//	final static int DB_PORT = 33069;
//	final static String DB_DESCRIPCION_VERSION = "Creaci�n inicial";
////	final static String DB_NAME = "INDICA LA BD";
////	final static String DB_USER = "INDICA EL USUARIO";
//	final static String DB_NAME = "acadt";
//	final static String DB_USER = "acadt";
//	final static String DB_PASSWORD = "dam2";

    final static int DB_VERSION = 1;
    final static String DB_DESCRIPCION_VERSION = "Creación inicial";
    final static String DB_NAME = "movieDickH2";
    final static String DB_USER = "SA";
    final static String DB_PASSWORD = "";

    private ConectaBD() throws ExceptionDataBase {
        estableceConexion();
    }

    public static ConectaBD getConectaBD() throws ExceptionDataBase {
        if (conectaBD == null) {
            conectaBD = new ConectaBD();
        }
        return conectaBD;
    }

    private void estableceConexion() {
        error = false;
        try {
            Class.forName("org.h2.Driver");
            //String urlAcceso = "jdbc:h2:~/+" + DB_NAME; //En directorio personal
            //String urlAcceso = "jdbc:h2:file:./+" + DB_NAME; // en directorio proyecto
            String urlAcceso = "jdbc:h2:file:./db/" + DB_NAME; //en directorio proyecto dentro de carpeta db

            connection = DriverManager.getConnection(urlAcceso, DB_USER, DB_PASSWORD);
            connection.setAutoCommit(false);
            stmtUpdate = connection.createStatement();
            update();
            connection.commit();
        } catch (ClassNotFoundException ex) {
            error = true;
            System.err.println("Driver h2 no encontrado");
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException ex1) {
            }
            error = true;
            System.err.println("Se ha producido un error en la sentencia SQL");
        }
    }

    private void update() {
        error = false;
        int versionBD = lastVersion();
        switch (versionBD) {
            case 0:
                try {
                    if (obtenerBDVersion() == 0) {
                        updateV0();
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
//			case 1 : updateV1();
        }
    }

    private void updateV0() throws SQLException {
        error = false;
        String sql;
        sql = "CREATE TABLE peliculas (" +
                " id INTEGER PRIMARY KEY AUTO_INCREMENT," +
                " id_pelicula VARCHAR(30) UNIQUE," +
                " titulo VARCHAR(100)," +
                " imagen_portada VARCHAR(300), " +
                " imagen_fondo VARCHAR(300)," +
                " descripcion TEXT," +
                " fecha DATE," +
                " UNIQUE(titulo,fecha)"+
                " );";
        stmtUpdate.execute(sql);

        sql = " CREATE TABLE idiomas (\n" +
                " id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                " id_idioma VARCHAR(30) UNIQUE, \n" +
                " nombre VARCHAR(50) UNIQUE\n" +

                ");";
        stmtUpdate.execute(sql);

        sql = "CREATE TABLE enlaces (\n" +
                " id INTEGER PRIMARY KEY AUTO_INCREMENT, \n" +
                " id_enlace VARCHAR(30) UNIQUE,\n" +
                " id_pelicula VARCHAR(30),\n" +
                " enlace VARCHAR(300) UNIQUE ,\n" +
                " idioma VARCHAR(30)\n" +
                " );";
        stmtUpdate.execute(sql);

        sql = "CREATE TABLE generos (\n" +
                " id INTEGER PRIMARY KEY AUTO_INCREMENT, \n" +
                " id_genero VARCHAR(30) UNIQUE,\n" +
                " nombre VARCHAR(60)\n UNIQUE" +
                " );";

        stmtUpdate.execute(sql);

        sql = "CREATE TABLE peliculas_generos (\n" +
                " id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                " id_pelicula VARCHAR(30), \n" +
                " id_genero VARCHAR(50) \n" +
                ");";

        stmtUpdate.execute(sql);

        sql = "ALTER TABLE enlaces ADD FOREIGN KEY (idioma) REFERENCES idiomas(id_idioma);\n" +
                "ALTER TABLE enlaces ADD FOREIGN KEY (id_pelicula) REFERENCES peliculas(id_pelicula);\n" +
                "\n" +
                "ALTER TABLE peliculas_generos ADD FOREIGN KEY (id_pelicula) REFERENCES peliculas(id_pelicula);\n" +
                "ALTER TABLE peliculas_generos ADD FOREIGN KEY (id_genero) REFERENCES generos(id_genero);";

        stmtUpdate.execute(sql);

        sql = "CREATE TABLE IF NOT EXISTS version(id INTEGER PRIMARY KEY AUTO_INCREMENT," +
                "version INTEGER NOT NULL," +
                "descripcion VARCHAR(50)," +
                "fecha TIMESTAMP," +
                "CONSTRAINT  version UNIQUE (version))";
        stmtUpdate.execute(sql);
        sql = "INSERT INTO version values(0," + DB_VERSION + ",'Version 1','2019-11-07')";
        stmtUpdate.execute(sql);
        //sql="PRAGMA foreign_key=on";
        //stmtUpdate.execute(sql);


    }

    private int obtenerBDVersion() {


        boolean errorVersion;
        errorVersion = false;
        int version = 0;
        try {

            String sql = "SELECT version FROM version ORDER BY version DESC LIMIT 1";
            ResultSet rs = stmtUpdate.executeQuery(sql);
            if (rs.next()) {
                version = rs.getInt("version");
            }
            rs.close();
        } catch (SQLException ex) {

            errorVersion = true;

        }
        return version;
    }

    public int lastVersion() {
        return 0;
    }

    public static void close() {
        if (conectaBD != null) {
            try {
                conectaBD.connection.close();
                conectaBD.connection = null;
            } catch (SQLException e) {
            }
        }
    }


    public Connection getConnection() {
        if (connection == null)
            estableceConexion();
        return connection;
    }

    public boolean isError() {
        return error;
    }

}
