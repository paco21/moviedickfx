package com.moviedick.fx.model.dao.tables;

import com.moviedick.fx.model.dao.GenericDao;
import com.moviedick.fx.model.entity.Idioma;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class IdiomasDao extends GenericDao<Idioma, Integer> {
    private static IdiomasDao idiomasDao;

    public final static String SORT_NOMBRE_ASC = "nombre ASC";
    public final static String SORT_IDIDIOMA_ASC = "id_idioma ASC";
    public final static String SORT_NOMBRE_DESC = "nombre DESC";
    public final static String SORT_IDIDIOMA_DESC = "id_idioma DESC";


    private IdiomasDao() {
        super();
        entityFiltro = new Idioma();
    }

    public static IdiomasDao connect() {
        if (idiomasDao == null) {
            idiomasDao = new IdiomasDao();
        }
        return idiomasDao;
    }


//------------------------------------------
// Funcionalidades extra	
//------------------------------------------	
public boolean deleteIdioma(String idPelicula){
    boolean correcto = true;
    String sqls[]={
            "DELETE FROM " + "ENLACES" + " WHERE idioma=?",
            "DELETE FROM " + nombreTabla() + " WHERE id_idioma=?"};
    try {
        executeDelete(idPelicula,sqls);
        getConnection().commit();
    } catch (SQLException ex) {
        correcto = false;
        error = true;
        try {
            getConnection().rollback();
        } catch (SQLException ex1) {
        }
    }
    return correcto;
}
//------------------------------------------
// M�todos auxiliares	
//------------------------------------------	

    @Override
    protected String nombreTabla() {
        return "idiomas";
    }

    @Override
    protected String[] itemsTabla() {
        String[] items = {"id", "id_idioma", "nombre"};
        return items;
    }

    @Override
    protected String generaSelect(Idioma entity) {
        String condicion = "WHERE 1 ";
        if (entity != null) {
            if (entity.getId() != null) {
                condicion += "AND id = " + entity.getId() + " ";
            }
            if (entity.getIdIdioma() != null) {
                condicion += "AND id_idioma = '" + entity.getIdIdioma() + "' ";
            }
            if (entity.getNombre() != null) {
                condicion += "AND nombre = '" + entity.getNombre() + "' ";
            }


        }
        String sql = "SELECT " + itemTablaToString() + " FROM idiomas " + condicion;
        return sql;
    }

    @Override
    protected Integer _add(Idioma entity) throws SQLException {
        Integer id = null;
        String sql = "INSERT INTO IDIOMAS " + "(ID_IDIOMA,NOMBRE) VALUES " + "(?,?)";
        if (entity != null) {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getIdIdioma());
            ps.setString(2, entity.getNombre());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                id = -1; // Si se ha a�adido pero no se ha devuelto un id
            }
        }
        return id;
    }

    @Override
    protected Idioma itemToEntity(ResultSet rs) throws SQLException {
        Idioma obj = new Idioma();
        obj.setId(rs.getInt("id"));
        obj.setIdIdioma(rs.getString("id_idioma"));
        obj.setNombre(rs.getString("nombre"));

        return obj;
    }

    @Override
    protected PreparedStatement generaUpdate(Idioma entity) throws SQLException {
        String sql = "UPDATE idiomas SET " + "ID_IDIOMA=?, NOMBRE=? " + "WHERE id=?";
        PreparedStatement ps = null;
        ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getIdIdioma());
        ps.setString(2, entity.getNombre());
        ps.setInt(3, entity.getId());
        return ps;
    }

}