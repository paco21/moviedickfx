package com.moviedick.fx.model.dao;

import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericDao<T, PK extends Serializable> implements IGenericDao<T, PK> {

    protected ConectaBD conectaBD;
    protected boolean error;
    protected Statement statement;
    protected int paginacionFiltro;
    protected T entityFiltro;
    protected String sortConsulta;

    protected GenericDao() {
        conecta();
    }

    public void conecta() {
        error = false;
        try {
            conectaBD = ConectaBD.getConectaBD();
            statement = conectaBD.getConnection().createStatement();
        } catch (SQLException ex) {
            error = true;
            System.err.println("Error en la conexion con la BD.");
        } catch (ExceptionDataBase ex) {
            error = true;
            System.err.println(ex.toString());
        }
    }

    public Connection getConnection() {
        return conectaBD.getConnection();
    }

    public Statement getStatement() {
        try {
            statement = conectaBD.getConnection().createStatement();
        } catch (SQLException e) {
            error = true;
            System.err.println("Error en la conexion con la BD.");
        }
        return statement;
    }

    public void close() {
        ConectaBD.close();
    }

    public void sortConsulta(String... ordenacion) {
        sortConsulta = "";

        for (String orden : ordenacion) {
            sortConsulta += orden + ", ";
        }
        if (sortConsulta.length() > 0) {
            sortConsulta = "ORDER BY " + sortConsulta.substring(0, sortConsulta.length() - 2);
        }

    }
    public void clearSortConsulta(){
        sortConsulta=null;
    }

    @Override
    public boolean isError() {
        return error;
    }

    @Override
    public T get(PK id) {
        error = false;
        T obj = null;
        if (id != null) {
            String sql = "SELECT " + itemTablaToString() + " FROM " + nombreTabla() + "  WHERE id =  " + id;
            try {
                ResultSet rsFiltro = getStatement().executeQuery(sql);
                if (rsFiltro.next()) {
                    obj = itemToEntity(rsFiltro);
                }
                rsFiltro.close();
            } catch (SQLException ex) {
                error = true;
            }
        }
        return obj;
    }

    /**
     * Realiza una consulta seg�n los campos completados
     *
     * @param entity
     * @return devuelve el primer registro que cumpla la condicion
     */
    @Override
    public T get(T entity) {
        entityFiltro = (T) entity;
        paginacionFiltro = 0;
        T obj = null;
        error = false;
        if (entity != null) {

            String sqlFiltro = generaConsultaCompleta(generaSelect(entityFiltro), sortConsulta, null);

            try {
                ResultSet rsFiltro = getStatement().executeQuery(sqlFiltro);
                if (rsFiltro.next()) {
                    obj = itemToEntity(rsFiltro);
                }
                rsFiltro.close();
            } catch (SQLException ex) {
                obj = null;
                error = true;
            }
        }

        return obj;
    }

    @Override
    public PK add(T entity) {
        PK id = null;
        error = false;
        if (entity != null) {
            try {
                id = _add(entity);
                getConnection().commit();

            } catch (SQLException ex) {
                id = null;
                error = true;
                try {
                    getConnection().rollback();
                } catch (SQLException ex1) {
                }
            }
        }
        return id;
    }

    @Override
    public boolean add(List<T> list) {
        boolean correcto = true;
        error = false;

        try {
            for (T entity : list) {
                _add(entity);
            }
            getConnection().commit();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            correcto = false;
            error = true;
            try {
                getConnection().rollback();
            } catch (SQLException ex1) {
            }
        }
        return correcto;
    }

    @Override
    public T next() {
        T obj = null;
        String sql = generaConsultaCompleta(generaSelect(entityFiltro), sortConsulta,
                " LIMIT 1 OFFSET " + ++paginacionFiltro);

        try {
            ResultSet rsFiltro = getStatement().executeQuery(sql);
            if (rsFiltro.next()) {
                obj = itemToEntity(rsFiltro);
            }
            rsFiltro.close();
        } catch (SQLException ex) {
            obj = null;
            error = true;
        }
        return obj;
    }

    @Override
    public T previous() {
        T obj = null;
        String sql = generaConsultaCompleta(generaSelect(entityFiltro), sortConsulta,
                " LIMIT 1 OFFSET " + --paginacionFiltro);

        try {
            ResultSet rsFiltro = getStatement().executeQuery(sql);
            if (rsFiltro.next()) {
                obj = itemToEntity(rsFiltro);

            }
            rsFiltro.close();
        } catch (SQLException ex) {
            obj = null;
            error = true;
        }
        return obj;
    }

    @Override
    public List<T> listAll() {
        List<T> lista = new ArrayList<T>();
        error = false;
        String sql = generaConsultaCompleta(generaSelect(null), sortConsulta, null);

        try {
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next()) {
                T participante = itemToEntity(rs);
                lista.add(participante);
            }
            rs.close();
        } catch (SQLException ex) {
            error = true;
        }
        return lista;
    }

    @Override
    public List<T> list(T filter, Integer rows) {
        entityFiltro = filter;
        List<T> lista = new ArrayList<T>();
        paginacionFiltro = 0;

        if (rows == null)
            rows = Integer.MAX_VALUE;

        String sql = generaConsultaCompleta(generaSelect(entityFiltro), sortConsulta, " LIMIT " + rows + " OFFSET 0");

        try {
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next()) {
                T obj = itemToEntity(rs);
                lista.add(obj);
            }
            paginacionFiltro = lista.size();
        } catch (SQLException ex) {
            error = true;
        }
        return lista;
    }

    @Override
    public List<T> listNext(Integer rows) {
        List<T> lista = new ArrayList<T>();
        error = false;

        if (rows == null)
            rows = Integer.MAX_VALUE;

        String sql = generaConsultaCompleta(generaSelect(entityFiltro), sortConsulta,
                " LIMIT " + rows + " OFFSET " + paginacionFiltro);

        try {
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next() && rows > 0) {
                T obj = itemToEntity(rs);
                lista.add(obj);
                rows--;
            }
            paginacionFiltro += lista.size();
        } catch (SQLException ex) {
            error = true;
        }
        return lista;

    }

    @Override
    public List<T> listPrevious(Integer rows) {
        List<T> lista = new ArrayList<T>();
        error = false;
        if (paginacionFiltro < rows) {
            rows = paginacionFiltro;
            paginacionFiltro = 0;
        } else {
            paginacionFiltro -= rows;
        }

        String sql = generaConsultaCompleta(generaSelect(entityFiltro), sortConsulta,
                " LIMIT " + rows + " OFFSET " + paginacionFiltro);

        try {
            ResultSet rs = getStatement().executeQuery(sql);
            while (rs.next() && rows > 0) {
                T obj = itemToEntity(rs);
                lista.add(obj);
                rows--;
            }
        } catch (SQLException ex) {
            error = true;
        }
        return lista;
    }

    @Override
    public boolean delete(PK id) {
        boolean correcto = true;
        String sql = "DELETE FROM " + nombreTabla() + " WHERE id=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, (Integer) id);
            ps.executeUpdate();
            getConnection().commit();
        } catch (SQLException ex) {
            correcto = false;
            error = true;
            try {
                getConnection().rollback();
            } catch (SQLException ex1) {
            }
        }
        return correcto;
    }

    @Override
    public boolean exist(PK id) {
        error = false;
        boolean encontrado = false;
        if (id != null) {
            String sql = "SELECT " + itemTablaToString() + "FROM " + nombreTabla() + "  WHERE id =  " + id;
            try {
                ResultSet rsFiltro = getStatement().executeQuery(sql);
                encontrado = rsFiltro.next();
                rsFiltro.close();
            } catch (SQLException ex) {
                error = true;
            }
        }
        return encontrado;
    }

    /**
     * Actualiza el registro que tiene el id
     *
     * @param entity
     * @return
     */
    @Override
    public boolean update(T entity) {
        boolean correcto = true;
        if (entity != null) {
            try {
                PreparedStatement ps = generaUpdate(entity);
                ps.executeUpdate();
                getConnection().commit();
            } catch (SQLException ex) {
                correcto = false;
                error = true;
                try {
                    getConnection().rollback();
                } catch (SQLException ex1) {
                }
            }
        }
        return correcto;
    }

    protected String generaConsultaCompleta(String select, String orderBy, String limit) {
        return select + " " + (orderBy != null ? orderBy : "") + " " + (limit != null ? limit : "");
    }

    protected String itemTablaToString() {
        String items = "";
        for (int i = 0; i < itemsTabla().length - 1; i++) {
            items += itemsTabla()[i] + ", ";
        }
        items += itemsTabla()[itemsTabla().length - 1];
        return items;
    }
    protected void executeDelete(String idEntity,String... sqls) throws SQLException {
        for (String sql:sqls) {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, idEntity);
            ps.executeUpdate();
        }
    }

    protected abstract String nombreTabla();

    protected abstract String[] itemsTabla();

    protected abstract T itemToEntity(ResultSet rs) throws SQLException;

    protected abstract PK _add(T entity) throws SQLException;

    protected abstract String generaSelect(T entity);

    protected abstract PreparedStatement generaUpdate(T entity) throws SQLException;

}
