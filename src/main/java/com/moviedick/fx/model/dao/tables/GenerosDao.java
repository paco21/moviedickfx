package com.moviedick.fx.model.dao.tables;

import com.moviedick.fx.model.dao.GenericDao;
import com.moviedick.fx.model.entity.Genero;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GenerosDao extends GenericDao<Genero, Integer> {
    private static GenerosDao generosDao;

    public final static String SORT_NOMBRE_ASC = "nombre ASC";
    public final static String SORT_IDGENERO_ASC = "id_genero ASC";
    public final static String SORT_NOMBRE_DESC = "nombre DESC";
    public final static String SORT_IDGENERO_DESC = "id_genero DESC";


    private GenerosDao() {
        super();
        entityFiltro = new Genero();
    }

    public static GenerosDao connect() {
        if (generosDao == null) {
            generosDao = new GenerosDao();
        }
        return generosDao;
    }


//------------------------------------------
// Funcionalidades extra
//------------------------------------------
public boolean deleteGenero(String idGenero){
    boolean correcto = true;
    String sqls[]={"DELETE FROM " + "PELICULAS_GENEROS" + " WHERE id_genero=?","DELETE FROM " + nombreTabla() + " WHERE id_genero=?"};
    try {
        executeDelete(idGenero,sqls);
        getConnection().commit();
    } catch (SQLException ex) {
        correcto = false;
        error = true;
        try {
            getConnection().rollback();
        } catch (SQLException ex1) {
        }
    }
    return correcto;
}
//------------------------------------------
// M�todos auxiliares
//------------------------------------------

    @Override
    protected String nombreTabla() {
        return "generos";
    }

    @Override
    protected String[] itemsTabla() {
        String[] items = {"id", "id_genero", "nombre"};
        return items;
    }

    @Override
    protected String generaSelect(Genero entity) {
        String condicion = "WHERE 1 ";
        if (entity != null) {
            if (entity.getId() != null) {
                condicion += "AND id = " + entity.getId() + " ";
            }
            if (entity.getIdGenero() != null) {
                condicion += "AND id_genero = '" + entity.getIdGenero() + "' ";
            }
            if (entity.getNombre() != null) {
                condicion += "AND nombre = '" + entity.getNombre() + "' ";
            }


        }
        String sql = "SELECT " + itemTablaToString() + " FROM generos " + condicion;
        return sql;
    }

    @Override
    protected Integer _add(Genero entity) throws SQLException {
        Integer id = null;
        String sql = "INSERT INTO GENEROS " + "(ID_GENERO,NOMBRE) VALUES " + "(?,?)";
        if (entity != null) {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getIdGenero());
            ps.setString(2, entity.getNombre());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                id = -1; // Si se ha a�adido pero no se ha devuelto un id
            }
        }
        return id;
    }

    @Override
    protected Genero itemToEntity(ResultSet rs) throws SQLException {
        Genero obj = new Genero();
        obj.setId(rs.getInt("id"));
        obj.setIdGenero(rs.getString("id_genero"));
        obj.setNombre(rs.getString("nombre"));

        return obj;
    }

    @Override
    protected PreparedStatement generaUpdate(Genero entity) throws SQLException {
        String sql = "UPDATE generos SET " + "ID_GENERO=?, NOMBRE=? " + "WHERE id=?";
        PreparedStatement ps = null;
        ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getIdGenero());
        ps.setString(2, entity.getNombre());
        ps.setInt(3, entity.getId());
        return ps;
    }

}