package com.moviedick.fx.model.dao.tables;

import com.moviedick.fx.model.dao.GenericDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.model.entity.PeliculaGenero;
import com.moviedick.fx.util.UtilFecha;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PeliculasDao extends GenericDao<Pelicula, Integer> {
    private static PeliculasDao peliculasDao;

    public final static String SORT_IDPELICULA_ASC = "id_pelicula ASC";
    public final static String SORT_TITULO_ASC = "titulo ASC";
    public final static String SORT_DESCRIPCION_ASC = "descripcion ASC";
    public final static String SORT_FECHA_ASC = "fecha ASC";
    public final static String SORT_ID_ASC = "id ASC";

    public final static String SORT_IDPELICULA_DESC = "id_pelicula DESC";
    public final static String SORT_TITULO_DESC = "titulo DESC";
    public final static String SORT_DESCRIPCION_DESC = "descripcion DESC";
    public final static String SORT_FECHA_DESC = "fecha DESC";
    public final static String SORT_ID_DES = "id DES";


    private PeliculasDao() {
        super();
        entityFiltro = new Pelicula();
    }

    public static PeliculasDao connect() {
        if (peliculasDao == null) {
            peliculasDao = new PeliculasDao();
        }
        return peliculasDao;
    }


//------------------------------------------
// Funcionalidades extra	
//------------------------------------------	
public boolean deletePelicula(String idPelicula){
    boolean correcto = true;
    String sqls[]={
            "DELETE FROM " + "PELICULAS_GENEROS" + " WHERE id_pelicula=?",
            "DELETE FROM " + "ENLACES" + " WHERE id_pelicula=?",
            "DELETE FROM " + nombreTabla() + " WHERE id_pelicula=?"};
    try {
        executeDelete(idPelicula,sqls);
        getConnection().commit();
    } catch (SQLException ex) {
        correcto = false;
        error = true;
        try {
            getConnection().rollback();
        } catch (SQLException ex1) {
        }
    }
    return correcto;
}

    public List<Pelicula> findByTitulo(String titulo) {
        List<Pelicula> lista = new ArrayList<Pelicula>();
        error = false;

            String sql = "SELECT "+itemTablaToString() +
                    " FROM "+nombreTabla() +
                    " WHERE UPPER(titulo) LIKE ?";
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(sql);
            ps.setString(1, '%'+titulo.toUpperCase()+'%');
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                lista.add(itemToEntity(rs));
            }
            rs.close();
        } catch (SQLException e) {
            error = true;
        }
        return lista;
    }


//------------------------------------------
// M�todos auxiliares	
//------------------------------------------	

    @Override
    protected String nombreTabla() {
        return "peliculas";
    }

    @Override
    protected String[] itemsTabla() {
        String[] items = {"id", "id_pelicula", "titulo", "imagen_portada", "imagen_fondo", "descripcion", "fecha"};
        return items;
    }

    @Override
    protected String generaSelect(Pelicula entity) {
        String condicion = "WHERE 1 ";
        if (entity != null) {
            if (entity.getId() != null) {
                condicion += "AND id = " + entity.getId() + " ";
            }
            if (entity.getIdPelicula() != null) {
                condicion += "AND id_pelicula = '" + entity.getIdPelicula() + "' ";
            }
            if (entity.getTitulo() != null) {
                condicion += "AND titulo = '" + entity.getTitulo() + "' ";
            }
            if (entity.getImagenPortada() != null) {
                condicion += "AND imagen_portada = '" + entity.getImagenPortada() + "' ";
            }
            if (entity.getImagenFondo() != null) {
                condicion += "AND imagen_fondo = '" + entity.getImagenFondo() + "' ";
            }
            if (entity.getDescripcion() != null) {
                condicion += "AND descripcion = '" + entity.getDescripcion() + "' ";
            }
            if (entity.getFecha() != null) {
                condicion += "AND fecha = '" + UtilFecha.LocalDateToStringMysql(entity.getFecha()) + "' ";
            }
        }
        String sql = "SELECT " + itemTablaToString() + " FROM peliculas " + condicion;
        return sql;
    }

    @Override
    protected Integer _add(Pelicula entity) throws SQLException {
        Integer id = null;
        String sql = "INSERT INTO PELICULAS " + "(id_pelicula, titulo, imagen_portada,imagen_fondo,descripcion, fecha) VALUES " + "(?,?,?,?,?,?)";
        if (entity != null) {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getIdPelicula());
            ps.setString(2, entity.getTitulo());
            ps.setString(3, entity.getImagenPortada());
            ps.setString(4, entity.getImagenFondo());
            ps.setString(5, entity.getDescripcion());
            if (entity.getFecha() != null)
                ps.setDate(6, UtilFecha.LocalDateToDateSql(entity.getFecha()));
            else
                ps.setNull(6, Types.TIMESTAMP);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                id = -1; // Si se ha a�adido pero no se ha devuelto un id
            }
        }
        return id;
    }

    @Override
    protected Pelicula itemToEntity(ResultSet rs) throws SQLException {
        Pelicula obj = new Pelicula();
        obj.setId(rs.getInt("id"));
        obj.setIdPelicula(rs.getString("id_pelicula"));
        obj.setTitulo(rs.getString("titulo"));
        obj.setImagenPortada(rs.getString("imagen_portada"));
        obj.setImagenFondo(rs.getString("imagen_fondo"));
        obj.setFecha(UtilFecha.DateSqlToLocalDate(rs.getDate("fecha")));
        obj.setDescripcion(rs.getString("descripcion"));
        return obj;
    }

    @Override
    protected PreparedStatement generaUpdate(Pelicula entity) throws SQLException {
        String sql = "UPDATE peliculas SET " + "id_pelicula=?, titulo=?, imagen_portada=?,imagen_fondo=?,descripcion=?, fecha=? " + "WHERE id=?";
        PreparedStatement ps = null;
        ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getIdPelicula());
        ps.setString(2, entity.getTitulo());
        ps.setString(3, entity.getImagenPortada());
        ps.setString(4, entity.getImagenFondo());
        ps.setString(5, entity.getDescripcion());
        ps.setDate(6, UtilFecha.LocalDateToDateSql(entity.getFecha()));
        ps.setInt(7, entity.getId());
        return ps;
    }

}