package com.moviedick.fx.model.dao.tables;

import com.moviedick.fx.model.dao.GenericDao;
import com.moviedick.fx.model.entity.Enlace;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.model.entity.Pelicula;

import java.sql.*;

public class EnlacesDao extends GenericDao<Enlace, Integer> {
    private static EnlacesDao enlacesDao;

    public final static String SORT_IDENLACE_ASC = "id_enlace ASC";
    public final static String SORT_IDPELICULA_ASC = "id_pelicula ASC";
    public final static String SORT_ENLACE_ASC = "enlace ASC";
    public final static String SORT_IDIOMA_ASC = "idioma ASC";

    public final static String SORT_IDENLACE_DESC = "id_enlace DESC";
    public final static String SORT_IDPELICULA_DESC = "id_pelicula DESC";
    public final static String SORT_ENLACE_DESC = "enlace DESC";
    public final static String SORT_IDIOMA_DESC = "idioma DESC";


    private EnlacesDao() {
        super();
        entityFiltro = new Enlace();
    }

    public static EnlacesDao connect() {
        if (enlacesDao == null) {
            enlacesDao = new EnlacesDao();
        }
        return enlacesDao;
    }


//------------------------------------------
// Funcionalidades extra	
//------------------------------------------	
public boolean deleteByIdPelicula(String idPelicula){
    boolean correcto = true;
    String sql = "DELETE FROM " + nombreTabla() + " WHERE id_pelicula=?";
    try {
        PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, idPelicula);
        ps.executeUpdate();
        getConnection().commit();
    } catch (SQLException ex) {
        correcto = false;
        error = true;
        try {
            getConnection().rollback();
        } catch (SQLException ex1) {
        }
    }
    return correcto;
}

    public boolean deleteByIdioma(String idIdioma){
        boolean correcto = true;
        String sql = "DELETE FROM " + nombreTabla() + " WHERE idioma=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, idIdioma);
            ps.executeUpdate();
            getConnection().commit();
        } catch (SQLException ex) {
            correcto = false;
            error = true;
            try {
                getConnection().rollback();
            } catch (SQLException ex1) {
            }
        }
        return correcto;
    }

//------------------------------------------
// M�todos auxiliares	
//------------------------------------------	

    @Override
    protected String nombreTabla() {
        return "enlaces";
    }

    @Override
    protected String[] itemsTabla() {
        String[] items = {"id", "id_enlace", "id_pelicula", "enlace", "idioma"};
        return items;
    }

    @Override
    protected String generaSelect(Enlace entity) {
        String condicion = "WHERE 1 ";
        if (entity != null) {
            if (entity.getId() != null) {
                condicion += "AND id = " + entity.getId() + " ";
            }
            if (entity.getIdEnlace() != null) {
                condicion += "AND id_enlace = '" + entity.getIdEnlace() + "' ";
            }
            if (entity.getPelicula() != null) {
                if (entity.getPelicula().getIdPelicula() != null) {
                    condicion += "AND id_pelicula = '" + entity.getPelicula().getIdPelicula() + "' ";
                }
            }
            if (entity.getEnlace() != null) {
                condicion += "AND enlace = '" + entity.getEnlace() + "' ";
            }
            if (entity.getIdioma() != null) {
                if (entity.getIdioma().getIdIdioma() != null) {
                    condicion += "AND idioma = '" + entity.getIdioma().getIdIdioma() + "' ";
                }
            }

        }
        String sql = "SELECT " + itemTablaToString() + " FROM enlaces " + condicion;
        return sql;
    }

    @Override
    protected Integer _add(Enlace entity) throws SQLException {
        Integer id = null;
        String sql = "INSERT INTO ENLACES " + "(ID_ENLACE,id_pelicula, ENLACE, IDIOMA) VALUES " + "(?,?,?,?)";
        if (entity != null) {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getIdEnlace());
            if (entity.getPelicula() != null) {
                if (entity.getPelicula().getIdPelicula() != null) {
                    ps.setString(2, entity.getPelicula().getIdPelicula());
                } else {
                    ps.setNull(2, Types.VARCHAR);
                }
            } else {
                ps.setNull(2, Types.VARCHAR);
            }
            ps.setString(3, entity.getEnlace());

            if (entity.getIdioma() != null) {
                if (entity.getIdioma().getId() != null) {
                    ps.setString(4, entity.getIdioma().getIdIdioma());
                } else {
                    ps.setNull(4, Types.VARCHAR);
                }
            } else {
                ps.setNull(4, Types.VARCHAR);
            }
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                id = -1; // Si se ha a�adido pero no se ha devuelto un id
            }
        }
        return id;
    }

    @Override
    protected Enlace itemToEntity(ResultSet rs) throws SQLException {
        Enlace obj = new Enlace();
        obj.setId(rs.getInt("id"));
        obj.setIdEnlace(rs.getString("id_enlace"));
        obj.setPelicula(PeliculasDao.connect().get(new Pelicula(rs.getString("id_pelicula"))));
        obj.setEnlace(rs.getString("enlace"));
        obj.setIdioma(IdiomasDao.connect().get(new Idioma(rs.getString("idioma"))));
        return obj;
    }

    @Override
    protected PreparedStatement generaUpdate(Enlace entity) throws SQLException {
        String sql = "UPDATE enlaces SET " + "id_enlace=?, id_pelicula=?, enlace=?,idioma=? " + "WHERE id=?";
        PreparedStatement ps = null;
        ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getIdEnlace());
        ps.setString(2, entity.getPelicula().getIdPelicula());
        ps.setString(3, entity.getEnlace());
        ps.setString(4, entity.getIdioma().getIdIdioma());
        ps.setInt(5, entity.getId());
        return ps;
    }

}