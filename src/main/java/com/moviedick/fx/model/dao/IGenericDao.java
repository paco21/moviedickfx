
package com.moviedick.fx.model.dao;

import java.io.Serializable;
import java.util.List;


public interface IGenericDao<T, PK extends Serializable> {
    T get(PK id);

    T get(T entity);

    PK add(T entity);

    boolean add(List<T> list);

    boolean update(T entity);

    boolean delete(PK id);

    T next();

    T previous();

    List<T> listAll();

    List<T> list(T filter, Integer rows);

    List<T> listNext(Integer rows);

    List<T> listPrevious(Integer rows);

    boolean exist(PK id);

    boolean isError();
}
