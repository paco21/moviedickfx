package com.moviedick.fx.model.dao.tables;

import com.moviedick.fx.model.dao.GenericDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.model.entity.PeliculaGenero;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PeliculasGenerosDao extends GenericDao<PeliculaGenero, Integer> {
    private static PeliculasGenerosDao generosDao;

    public final static String SORT_IDPELICULA_ASC = "id_pelicula ASC";
    public final static String SORT_IDGENERO_ASC = "id_genero ASC";
    public final static String SORT_IDPELICULA_DESC = "id_pelicula DESC";
    public final static String SORT_IDGENERO_DESC = "id_genero DESC";


    private PeliculasGenerosDao() {
        super();
        entityFiltro = new PeliculaGenero();
    }

    public static PeliculasGenerosDao connect() {
        if (generosDao == null) {
            generosDao = new PeliculasGenerosDao();
        }
        return generosDao;
    }


//------------------------------------------
// Funcionalidades extra
//------------------------------------------
    public boolean deleteByIdPelicula(String idPelicula){
        boolean correcto = true;
        String sql = "DELETE FROM " + nombreTabla() + " WHERE id_pelicula=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, idPelicula);
            ps.executeUpdate();
            getConnection().commit();
        } catch (SQLException ex) {
            correcto = false;
            error = true;
            try {
                getConnection().rollback();
            } catch (SQLException ex1) {
            }
        }
        return correcto;
    }

    public boolean deleteByIdGenero(String idGenero){
        boolean correcto = true;
        String sql = "DELETE FROM " + nombreTabla() + " WHERE id_genero=?";
        try {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, idGenero);
            ps.executeUpdate();
            getConnection().commit();
        } catch (SQLException ex) {
            correcto = false;
            error = true;
            try {
                getConnection().rollback();
            } catch (SQLException ex1) {
            }
        }
        return correcto;
    }

    public List<Pelicula> listAllFilterGenero(List<Genero> generos) {
        List<Pelicula> lista = new ArrayList<Pelicula>();
        error = false;
        String condicion="";
        for (Genero genero: generos
        ) {
            condicion+=" AND id_pelicula IN (SELECT ID_PELICULA" +
                    " FROM PELICULAS_GENEROS WHERE" +
                    " ID_GENERO='"+genero.getIdGenero()+"')";
        }
        if(!condicion.equals("")) {
            String sql = "SELECT DISTINCT(ID_PELICULA)" +
                    " FROM PELICULAS_GENEROS" +
                    " WHERE 1" + condicion;
            try {
                ResultSet rs = getStatement().executeQuery(sql);

                while (rs.next()) {
                    lista.add(itemToEntityExtra(rs));
                }
                rs.close();
            } catch (SQLException ex) {
                error = true;
            }
        }else{
            lista=PeliculasDao.connect().listAll();
        }
        return lista;
    }

    public List<Genero> listAllFilterGenerosByIdPelicula(String idPelicula) {
        List<Genero> lista = new ArrayList<Genero>();
        error = false;
            String sql = "SELECT ID_GENERO" +
                    " FROM PELICULAS_GENEROS" +
                    " WHERE id_pelicula='"+idPelicula+"'";
            try {
                ResultSet rs = getStatement().executeQuery(sql);

                while (rs.next()) {
                    lista.add(GenerosDao.connect().get(new Genero(rs.getString("id_genero"))));
                }
                rs.close();
            } catch (SQLException ex) {
                error = true;
            }
        return lista;
    }
//------------------------------------------
// M�todos auxiliares
//------------------------------------------

    @Override
    protected String nombreTabla() {
        return "peliculas_generos";
    }

    @Override
    protected String[] itemsTabla() {
        String[] items = {"id", "id_pelicula", "id_genero"};
        return items;
    }

    @Override
    protected String generaSelect(PeliculaGenero entity) {
        String condicion = "WHERE 1 ";
        if (entity != null) {
            if (entity.getId() != null) {
                condicion += "AND id = " + entity.getId() + " ";
            }
            if (entity.getPelicula() != null) {
                if (entity.getPelicula().getIdPelicula() != null) {
                    condicion += "AND id_pelicula = '" + entity.getPelicula().getIdPelicula() + "' ";
                }
            }
            if (entity.getGenero() != null) {
                if (entity.getGenero().getIdGenero() != null) {
                    condicion += "AND id_genero = '" + entity.getGenero().getIdGenero() + "' ";
                }
            }


        }
        String sql = "SELECT " + itemTablaToString() + " FROM peliculas_generos " + condicion;
        return sql;
    }

    @Override
    protected Integer _add(PeliculaGenero entity) throws SQLException {
        Integer id = null;
        String sql = "INSERT INTO PELICULAS_GENEROS " + "(ID_PELICULA,ID_GENERO) VALUES " + "(?,?)";
        if (entity != null) {
            PreparedStatement ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getPelicula().getIdPelicula());
            ps.setString(2, entity.getGenero().getIdGenero());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            } else {
                id = -1; // Si se ha a�adido pero no se ha devuelto un id
            }
        }
        return id;
    }

    @Override
    protected PeliculaGenero itemToEntity(ResultSet rs) throws SQLException {
        PeliculaGenero obj = new PeliculaGenero();
        obj.setId(rs.getInt("id"));
        obj.setPelicula(PeliculasDao.connect().get(new Pelicula(rs.getString("id_pelicula"))));
        obj.setGenero(GenerosDao.connect().get(new Genero(rs.getString("id_genero"))));

        return obj;
    }

    private Pelicula itemToEntityExtra(ResultSet rs) throws SQLException {
        return PeliculasDao.connect().get(new Pelicula(rs.getString("id_pelicula")));
    }

    @Override
    protected PreparedStatement generaUpdate(PeliculaGenero entity) throws SQLException {
        String sql = "UPDATE PELICULAS_GENEROS SET " + "ID_PELICULA=?,ID_GENERO=? " + "WHERE id=?";
        PreparedStatement ps = null;
        ps = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getPelicula().getIdPelicula());
        ps.setString(2, entity.getGenero().getIdGenero());
        ps.setInt(3, entity.getId());
        return ps;
    }

}