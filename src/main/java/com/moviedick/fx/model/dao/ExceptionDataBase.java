package com.moviedick.fx.model.dao;

/**
 * @author JAVIER
 */
public class ExceptionDataBase extends Exception {
    private static final long serialVersionUID = 1L;
    final static int DB_VERSION_EXCEPTION = 1;

    public ExceptionDataBase(String msg) {
        super(msg);
    }
}
