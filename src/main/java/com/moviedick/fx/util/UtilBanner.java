package com.moviedick.fx.util;

import javafx.application.Platform;
import javafx.scene.control.Label;

public class UtilBanner {
	
	public static void mensajeTemporal(Label display, String mensaje) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				display.setText(mensaje);
			}			
		});
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(5000);
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							display.setText("");
						}			
					});	
				} catch (InterruptedException e) {
				}
							
			}
			
		}).start();
	}

}
