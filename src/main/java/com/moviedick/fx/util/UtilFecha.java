package com.moviedick.fx.util;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class UtilFecha {
    public static String LocalDateToString(LocalDate fecha) {
        String date = null;
        try {
            date = fecha.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } catch (Exception ex) {
        }
        return date;
    }

    public static LocalDate StringToLocalDateTo(String strFecha) {
        LocalDate date = null;
        try {
            date = LocalDate.parse(strFecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } catch (Exception ex) {
        }
        return date;
    }

    public static LocalDate StringToLocalDateToYY(String strFecha) {
        LocalDate date = null;
        try {
            date = LocalDate.parse(strFecha, DateTimeFormatter.ofPattern("dd/MM/yy"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return date;
    }

    public static String LocalDateToStringMysql(LocalDate fecha) {
        String date = null;
        try {
            date = fecha.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (Exception ex) {
        }
        return date;
    }

    public static java.sql.Date LocalDateToDateSql(LocalDate fecha) {
        java.sql.Date date = null;
        try {
            date = java.sql.Date.valueOf(fecha);
        } catch (Exception ex) {
        }
        return date;
    }

    public static LocalDate DateSqlToLocalDate(java.sql.Date fecha) {
        LocalDate date = null;
        try {
            date = fecha.toLocalDate();
        } catch (Exception ex) {
        }
        return date;
    }

    public static boolean TimeLocalOK() {
      Boolean ok = true;
        LocalDateTime ntp=ObtenerFechaNTP.getNTPDate();

        LocalDateTime local= LocalDateTime.now();
        Instant instant = Instant.now(); //can be LocalDateTime
        ZoneId systemZone = ZoneId.systemDefault();//my timezone
        ZoneOffset currentOffsetForMyZone = systemZone.getRules().getOffset(instant);
        Long resto= ntp.toEpochSecond(currentOffsetForMyZone)-local.toEpochSecond(currentOffsetForMyZone);
        if ( !(resto<30 && resto>-30)){
            System.out.println("Actualize la hora");
            ok=false;
        }
        return ok;
    }

}
