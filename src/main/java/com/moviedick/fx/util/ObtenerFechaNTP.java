package com.moviedick.fx.util;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.net.InetAddress;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

//Importamos las librerias de Apache Commons
public class ObtenerFechaNTP {


    public static LocalDateTime getNTPDate() {
        String servidor = "time.windows.com";
        LocalDateTime fechaRecibida = null;

        NTPUDPClient cliente = new NTPUDPClient();
        //Tiempo de Espera Antes De Mandar Error.
        cliente.setDefaultTimeout(5000);
        try {
            InetAddress hostAddr = InetAddress.getByName(servidor);

            TimeInfo fecha = cliente.getTime(hostAddr);
            fechaRecibida =LocalDateTime.ofInstant(Instant.ofEpochMilli(fecha.getMessage().getTransmitTimeStamp().getTime()), ZoneId.systemDefault());
        } catch (Exception e) {
            System.err.println("Error "+e.getMessage());
        }
        //Cerramos la comunicación con el cliente

        cliente.close();

        return fechaRecibida == null ? null : fechaRecibida ;

    }


}
