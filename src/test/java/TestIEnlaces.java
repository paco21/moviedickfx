import com.moviedick.fx.model.dao.tables.EnlacesDao;
import com.moviedick.fx.model.dao.tables.IdiomasDao;
import com.moviedick.fx.model.dao.tables.PeliculasDao;
import com.moviedick.fx.model.entity.Enlace;
import com.moviedick.fx.model.entity.Idioma;
import com.moviedick.fx.model.entity.Pelicula;

public class TestIEnlaces {
    public static void main(String[] args) {
        //Añadir
        Enlace p = new Enlace();
        p.setIdEnlace(p.generarId());
        p.setPelicula(PeliculasDao.connect().get(new Pelicula("20191209-1746463964574000000")));
        p.setEnlace("https://www.youtube.com/watch?v=B3cJXk9IaH0");
        p.setIdioma(IdiomasDao.connect().get(new Idioma("20191209-19143769277315000000")));

        EnlacesDao.connect().add(p);
        for (Enlace enlace : EnlacesDao.connect().listAll()) {
            System.out.println(enlace);
        }
        //Modificar

        Enlace p01 = EnlacesDao.connect().get(new Enlace(p.getIdEnlace()));
        p01.setEnlace("Modificado");
        EnlacesDao.connect().update(p01);
        System.out.println("Modificando\n\n");
        for (Enlace enlace : EnlacesDao.connect().listAll()) {
            System.out.println(enlace);
        }
        //Borrando
        EnlacesDao.connect().delete(EnlacesDao.connect().get(new Enlace(p.getIdEnlace())).getId());
        System.out.println("Borrando\n\n");
        for (Enlace enlace : EnlacesDao.connect().listAll()) {
            System.out.println(enlace);
        }

    }
}
