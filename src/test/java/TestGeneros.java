import com.moviedick.fx.model.dao.tables.GenerosDao;
import com.moviedick.fx.model.entity.Genero;

public class TestGeneros {
    public static void main(String[] args) throws InterruptedException {
        //Añadir 10000 elementos prueba del generar ID
//        for (int i = 0; i <10000; i++) {
//            Genero p = new Genero();
//            p.setIdGenero(p.generarId());
//            Thread.sleep(0, 1);
//            p.setNombre("Accion");
//            GenerosDao.connect().add(p);
//        }

        //System.out.println(System.getProperty("user.dir")); obtener ruta del proyecto
            Genero p = new Genero();
            p.setIdGenero(p.generarId());
            p.setNombre("Accion");
            GenerosDao.connect().add(p);
            for (Genero genero : GenerosDao.connect().listAll()) {
                System.out.println(genero);
            }
           // Modificar

            Genero p01 = GenerosDao.connect().get(new Genero(p.getIdGenero()));
            p01.setNombre("Suspense");
            GenerosDao.connect().update(p01);
            System.out.println("Modificando\n\n");
            for (Genero genero : GenerosDao.connect().listAll()) {
                System.out.println(genero);
            }
           // Borrando
            GenerosDao.connect().delete(GenerosDao.connect().get(new Genero(p.getIdGenero())).getId());
            System.out.println("Borrando\n\n");
            for (Genero genero : GenerosDao.connect().listAll()) {
                System.out.println(genero);
            }


    }
}
