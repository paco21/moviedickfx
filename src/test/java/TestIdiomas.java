import com.moviedick.fx.model.dao.tables.IdiomasDao;
import com.moviedick.fx.model.entity.Idioma;

public class TestIdiomas {
    public static void main(String[] args) {
        //Añadir
        Idioma p = new Idioma();
        p.setIdIdioma(p.generarId());
        p.setNombre("Español");
        IdiomasDao.connect().add(p);
        for (Idioma idioma : IdiomasDao.connect().listAll()) {
            System.out.println(idioma);
        }
        //Modificar

        Idioma p01 = IdiomasDao.connect().get(new Idioma(p.getIdIdioma()));
        p01.setNombre("Español \uD83C\uDDEA\uD83C\uDDF8");
        IdiomasDao.connect().update(p01);
        System.out.println("Modificando\n\n");
        for (Idioma idioma : IdiomasDao.connect().listAll()) {
            System.out.println(idioma);
        }
        //Borrando
        IdiomasDao.connect().delete(IdiomasDao.connect().get(new Idioma(p.getIdIdioma())).getId());
        System.out.println("Borrando\n\n");
        for (Idioma idioma : IdiomasDao.connect().listAll()) {
            System.out.println(idioma);
        }

    }
}
