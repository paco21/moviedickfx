import com.moviedick.fx.model.dao.tables.PeliculasGenerosDao;
import com.moviedick.fx.model.entity.Genero;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.model.entity.PeliculaGenero;

public class TestPeliculasGeneros {
    public static void main(String[] args) throws InterruptedException {


            PeliculaGenero p = new PeliculaGenero();
            p.setPelicula(new Pelicula("20191209-1746463964574000000"));
            p.setGenero(new Genero("20191212-2113117639197800"));
        Integer id=PeliculasGenerosDao.connect().add(p);
            for (PeliculaGenero peliculaGenero : PeliculasGenerosDao.connect().listAll()) {
                System.out.println(peliculaGenero);
            }
           // Modificar

        PeliculaGenero p01 = PeliculasGenerosDao.connect().get(new PeliculaGenero(id));
            p01.setPelicula(new Pelicula("20191210-1746463964574000000"));
        PeliculasGenerosDao.connect().update(p01);
            System.out.println("Modificando\n\n");
            for (PeliculaGenero peliculaGenero : PeliculasGenerosDao.connect().listAll()) {
                System.out.println(peliculaGenero);
            }
           // Borrando
        PeliculasGenerosDao.connect().delete(id);
            System.out.println("Borrando\n\n");
            for (PeliculaGenero peliculaGenero : PeliculasGenerosDao.connect().listAll()) {
                System.out.println(peliculaGenero);
            }


    }
}
