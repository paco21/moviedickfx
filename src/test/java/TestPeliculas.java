import com.moviedick.fx.model.dao.tables.PeliculasDao;
import com.moviedick.fx.model.entity.Pelicula;
import com.moviedick.fx.util.UtilFecha;

public class TestPeliculas {
    public static void main(String[] args) {
        //Añadir
        Pelicula p = new Pelicula();
        p.setIdPelicula(p.generarId());
        p.setTitulo("El irlandés");
        p.setImagenPortada("https://image.tmdb.org/t/p/original/sNilmIAwBbH0DmBOWBS6gWvNDex.jpg");
        p.setImagenFondo("https://image.tmdb.org/t/p/original/1RDto0tLo8Fhq7OcwgDaM7nECb7.jpg");
        p.setDescripcion("Pennsylvania, 1956. Frank Sheeran, un veterano de guerra de origen irlandés que trabaja como camionero, conoce accidentalmente al mafioso Russell Bufalino. Una vez convertido en su hombre de confianza, Bufalino envía a Frank a Chicago con el encargo de ayudar a Jimmy Hoffa, un poderoso líder sindical relacionado con el crimen organizado, con quien Frank mantendrá una estrecha amistad durante casi veinte años.");
        p.setFecha(UtilFecha.StringToLocalDateToYY("01/11/19"));
        PeliculasDao.connect().add(p);
        for (Pelicula pelicula : PeliculasDao.connect().listAll()) {
            System.out.println(pelicula);
        }
        //Modificar

        Pelicula p01 = PeliculasDao.connect().get(new Pelicula(p.getIdPelicula()));
        p01.setTitulo("Probando modificacion");
        PeliculasDao.connect().update(p01);
        System.out.println("Modificando\n\n");
        for (Pelicula pelicula : PeliculasDao.connect().listAll()) {
            System.out.println(pelicula);
        }
        //Borrando
        PeliculasDao.connect().delete(PeliculasDao.connect().get(new Pelicula(p.getIdPelicula())).getId());
        System.out.println("Borrando\n\n");
        for (Pelicula pelicula : PeliculasDao.connect().listAll()) {
            System.out.println(pelicula);
        }

    }
}
